#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "WritingSuspend (three)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 197 # 0.0 dB; why the heck are the amps so much fainter here?
# amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1

# source ./scripts/run-ldr.sh
# sleep 1

source ./scripts/run.sh --main-gain 10.0 --reso-gain -27.0

#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "WritingSuspend (shutdown dispatcher)"

sleep 3

java -cp shutdown/writingsuspend-shutdown.jar de.sciss.writing.Shutdown "$@"

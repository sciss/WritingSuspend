#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "WritingSuspend (two)"

sleep 5
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 27 # -32 dB
# amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1

# source ./scripts/run-ldr.sh
# sleep 1

# source ./scripts/run.sh --reso-gain -28.0
export WIRINGPI_GPIOMEM=1
java -cp common/writingsuspend-common.jar de.sciss.writing.WritingSuspend --nodes 1 --node-id 1 --noservos --min-phrase-ins-dur 0.66 --max-phrase-ins-dur 4.0 --start-muted
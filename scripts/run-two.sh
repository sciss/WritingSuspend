#!/bin/bash
cd "$(dirname "$0")"
cd ..

echo "WritingSuspend (two)"

sleep 2
source ./scripts/init-sound.sh
amixer -c1 set Speaker Playback 165 # -6 dB
# amixer -c1 set Mic Capture 18
sleep 8
source ./scripts/mount-ram-disk.sh
sleep 1

# source ./scripts/run-ldr.sh
# sleep 1

source ./scripts/run.sh --reso-gain -28.0

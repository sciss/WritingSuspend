/*
 *  SoloButtonControl.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.proc.Control
import de.sciss.writing.WritingSuspend.T

object SoloButtonControl {
  def apply()(implicit tx: T): Control[T] = {
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph._
      import de.sciss.proc.ExImport._
      val muted     = "mute".attr(false)
      val pinLED    = RPi.Pin(1)
//      val lb        = LoadBang()
//      val initBlink = Var(false)
      val stateLED  = muted // || initBlink
      /*val outLED    =*/ GPIO.DigitalOut(pinLED, stateLED)
      val pinBut1   = RPi.Pin(3)
      val pinBut2   = RPi.Pin(4)
      val inBut1    = GPIO.DigitalIn(pinBut1, pull = Some(true), init = true, debounce = 20)
      val inBut2    = GPIO.DigitalIn(pinBut2, pull = Some(true), init = true, debounce = 20)
      val but1Pr    = (!inBut1).toTrig
      val but2Rls   = inBut2.toTrig
      val but2Pr    = (!inBut2).toTrig
      but1Pr --> muted.set(!muted)
      val dlyShut = Delay(3.0)
      but2Pr  --> dlyShut
      but2Rls --> dlyShut.cancel
      val self = ThisRunner()
      dlyShut --> self.done
//      lb --> Act(
//        initBlink.set(true),
//        Delay(1.0)(initBlink.set(false))
//      )
    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}

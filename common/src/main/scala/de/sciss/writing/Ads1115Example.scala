package de.sciss.writing

/*
 * #%L
 * **********************************************************************
 * ORGANIZATION  :  Pi4J
 * PROJECT       :  Pi4J :: Java Examples
 * FILENAME      :  ADS1115GpioExample.java
 *
 * This file is part of the Pi4J project. More information about
 * this project can be found here:  https://www.pi4j.com/
 * **********************************************************************
 * %%
 * Copyright (C) 2012 - 2019 Pi4J
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 *//*
 * #%L
 * **********************************************************************
 * ORGANIZATION  :  Pi4J
 * PROJECT       :  Pi4J :: Java Examples
 * FILENAME      :  ADS1115GpioExample.java
 *
 * This file is part of the Pi4J project. More information about
 * this project can be found here:  https://www.pi4j.com/
 * **********************************************************************
 * %%
 * Copyright (C) 2012 - 2019 Pi4J
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import com.pi4j.gpio.extension.ads.ADS1x15GpioProvider.ProgrammableGainAmplifierValue
import com.pi4j.gpio.extension.ads.{ADS1115GpioProvider, ADS1115Pin}
import com.pi4j.io.gpio.{GpioFactory, GpioPinAnalogInput}
import com.pi4j.io.i2c.I2CBus

import java.text.DecimalFormat
import java.util.{Timer, TimerTask}
import scala.util.control.NonFatal

/**
 * <p>
 * This example code demonstrates how to use the ADS1115 Pi4J GPIO interface
 * for analog input pins.
 * </p>
 *
 * @author Robert Savage, Hanns Holger Rutz
 */
object ADS1115GpioExample {
  def main(args: Array[String]): Unit = {
    System.out.println("<--Pi4J--> ADS1115 GPIO Example ... started.")
    // number formatters
    val df  = new DecimalFormat("#.##")
    val pdf = new DecimalFormat("###.#")
    // create gpio controller
    val gpio = GpioFactory.getInstance
    // create custom ADS1115 GPIO provider
    val adc1 = new ADS1115GpioProvider(I2CBus.BUS_1, ADS1115GpioProvider.ADS1115_ADDRESS_0x48, false)
    val adc2 = new ADS1115GpioProvider(I2CBus.BUS_1, ADS1115GpioProvider.ADS1115_ADDRESS_0x49, false)
    // provision gpio analog input pins from ADS1115
    for i <- 0 until 0 do {
      val pin = ADS1115Pin.ALL(i % 4)
      val adc = if i < 4 then adc1 else adc2
      gpio.provisionAnalogInputPin(adc, pin, s"analog-in-$i")
    }
    // ATTENTION !!
    // It is important to set the PGA (Programmable Gain Amplifier) for all analog input pins.
    // (You can optionally set each input to a different value)
    // You measured input voltage should never exceed this value!
    //
    // In my testing, I am using a Sharp IR Distance Sensor (GP2Y0A21YK0F) whose voltage never exceeds 3.3 VDC
    // (http://www.adafruit.com/products/164)
    //
    // PGA value PGA_4_096V is a 1:1 scaled input,
    // so the output values are in direct proportion to the detected voltage on the input pins
    adc1.setProgrammableGainAmplifier(ProgrammableGainAmplifierValue.PGA_4_096V, ADS1115Pin.ALL: _*)
    adc2.setProgrammableGainAmplifier(ProgrammableGainAmplifierValue.PGA_4_096V, ADS1115Pin.ALL: _*)
    // Define a threshold value for each pin for analog value change events to be raised.
    // It is important to set this threshold high enough so that you don't overwhelm your program with change events for insignificant changes

    implicit val sc: SensorBody.Config = SensorBody.ConfigImpl()
    val sensorBody = SensorBody()
    SensorBody.ui(sensorBody)

    val timer = new Timer("ads", true)
    val timerFreq = 10 // Hz
    val period = 1000 / timerFreq
    timer.scheduleAtFixedRate(new TimerTask {
      // private val tStop = System.currentTimeMillis() + 60000

      override def run(): Unit = {
        // val buffer = new Array[Byte](1)
        val device = adc1.getDevice
        val raw    = new Array[Int](8)

        if false /*System.currentTimeMillis() > tStop*/ then {
          gpio.shutdown()
          System.out.println("Exiting ADS1115GpioExample")
          sys.exit()

        } else {
          // read device pins state
          device.read() // read(buffer, 0, 1)

          var i = 0
          while i < 8 do {
            val pin = ADS1115Pin.ALL(i % 4)
            try {
              val adc   = if i < 4 then adc1 else adc2
              val value = adc.getImmediateValue(pin)
              // val percent = (value * 100) / ADS1115GpioProvider.ADS1115_RANGE_MAX_VALUE
              // approximate voltage ( *scaled based on PGA setting )
              // val voltage = gpioProvider.getProgrammableGainAmplifier(pin).getVoltage * (percent / 100)
              // display output
              // System.out.println(" (" + pin.getName + ") : VOLTS=" + df.format(voltage) + "  | PERCENT=" + pdf.format(percent) + "% | RAW=" + value + "       ")
              raw(i) = value
              Thread.sleep(0x08)  // XXX TODO test

            } catch {
              case NonFatal(ex) =>
                ex.printStackTrace()
            }

            i += 1
          }
          sensorBody.update(raw)
        }
      }
    }
      , period, period)

      new Thread() {
        setDaemon(false)

        override def run(): Unit = this.synchronized(this.wait())

        start()
      }
  }
}


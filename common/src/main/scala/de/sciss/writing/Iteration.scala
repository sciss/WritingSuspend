/*
 *  Iteration.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.proc.Control
import de.sciss.writing.WritingSuspend.{ChansPerNode, Config, T}

object Iteration {
  def apply()(implicit tx: T, config: Config): Control[T] = {
    import config.{verbose, debug}
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph._
      import de.sciss.proc.ExImport._

      val self          = ThisRunner()
      val rSelect       = Runner("select")
      val rPlay         = Runner("play-phrase")
      val rReso         = Runner("resonance")
      val rSoloBut      = Runner("solo-buttons")
      val rng           = Random()

      val chan          = "chan".attr(0)
      val chanVr        = Var(0); chanVr --> chan    // cf. Lucre issue #70
      val iterCount     = Var(0)

      // --- OSC ---
      val portSeq       = "port-folder".attr(Folder()).children.select[Int]
      val hostSeq       = "host-folder".attr(Folder()).children.select[String]
      val numNodes      = portSeq.size
      val socketSeq     = (hostSeq zip portSeq).map { tup => SocketAddress(tup._1, tup._2) }
      val nodeIdx       = "node-idx".attr(0)
      val nextNodeIdx   = (nodeIdx + 1) % numNodes
//      val dummySocket: Ex[(String, Int)] = ("", -1)
      val thisHost      = hostSeq(nodeIdx     )
      val thisPort      = portSeq(nodeIdx     )
      val nextHost      = hostSeq(nextNodeIdx )
      val nextPort      = portSeq(nextNodeIdx )
      val oscNode       = OscUdpNode(thisPort, thisHost)
      if verbose then oscNode.dump_=(1)
      val trRelay       = oscNode.select("/relay", iterCount)
      val actRelay      = oscNode.send(SocketAddress(nextHost, nextPort), OscMessage("/relay", iterCount))

      // always respond
      oscNode.select("/ping") --> oscNode.send(oscNode.sender, OscMessage("/pong"))
      
      // quit
      oscNode.select("/shutdown") --> self.done
      rSoloBut.done --> self.done

      def mkHandshake(): (Act, Trig) = {
        val portsRemain   = Var(numNodes)
        val nextPort      = portSeq(portsRemain - 1)
        val nextHost      = hostSeq(portsRemain - 1)
        val done          = Trig()
        val timeOut       = Delay(3.0)
        val ping: Act     = If (portsRemain sig_== 0) Then done Else {
          Act(
            if debug then PrintLn("handshake to " ++ nextHost ++ ":" ++ nextPort.toStr) else Act.Nop(),
            timeOut,
            oscNode.send(SocketAddress(nextHost, nextPort), OscMessage("/ping"))
          )
        }
        oscNode.select("/pong") --> (If (oscNode.sender.port sig_== nextPort) Then Act(
          timeOut.cancel,
          portsRemain.set(portsRemain - 1),
          PrintLn("portsRemain NOW " ++ portsRemain.toStr),
          ping
        ))
        timeOut --> ping
        (ping, done)
      }

      val dbSeq         = "db-folder".attr(Folder()).children.select[AudioCue]
      val circleOff     = "circle-off".attr(0)
      val chansPerNode  = "chans-per-node".attr(8)
      val circleIdx     = circleOff + chanVr
      val db0           = dbSeq.apply(circleIdx)
      val maxDbDur      = "max-db-dur".attr(60.0)
      val dbAdvanceStep = "db-advance-step".attr(12.0)
      val SRd           = db0.sampleRate
      val dbDur         = db0.numFrames / SRd
      val dbMaxAdvance  = (dbDur - maxDbDur).max(0.0)
      val dbAdvance     = Var(0.0)
//      val dbAdvance     = (iterCount * dbAdvanceStep) % dbMaxAdvance
      val dbAdvanceD    = (dbAdvance * SRd        ).toLong
      val dbAdvanceT    = (dbAdvance * SampleRate ).toLong
      val db            = AudioCue(db0.artifact, db0.spec, offset = dbAdvanceT, gain = db0.gain)
      val init          = LoadBang()
//      val busyPlay      = self.attr[Boolean]("busy-play")
      val busyPlay      = "busy-play"   .attr(false)
      val busyRec       = "busy-rec"    .attr(false)
      val busyRender    = "busy-render" .attr(false)
      val busyRecVr     = Var(false); busyRecVr     --> busyRec    // cf. Lucre issue #70
      val busyRenderVr  = Var(false); busyRenderVr  --> busyRender // cf. Lucre issue #70
      val idle          = Var(true)

      val shouldPlay    = Var(false)

      val resOvrSpan    = Var[Span]()
      val resOvrNewLen  = Var[Long]()
      val resInsPos     = Var[Long]()

      def mkSendOverwrite(): (Act, Trig) = {
        val m             = OscMessage("/write", iterCount, circleIdx,
          resOvrSpan.start + dbAdvanceD, resOvrSpan.stop + dbAdvanceD, resOvrNewLen, resInsPos)
        val done          = Trig()
        val timeOut       = Delay(20.0)
        val countRemain   = Var(0)
        val vrIterBack    = Var(0)
        val iterCountL    = Var(0)
        oscNode.select("/written", vrIterBack) --> (If (vrIterBack sig_== iterCountL) Then Act(
          countRemain.set(countRemain - 1),
          if debug then PrintLn("send-ovr: count remain = " ++ countRemain.toStr) else Act.Nop(),
          If (countRemain sig_== 0) Then Act(
            timeOut.cancel,
            done
          )
        ))
        val sendAll = Act(
          if verbose then PrintLn("send-overwrite db advance " ++ dbAdvanceD.toStr) else Act.Nop(),
          iterCountL .set(iterCount), // "latch". XXX TODO is this needed?
          countRemain.set(numNodes),
          timeOut,
          socketSeq.map { tgt =>
            oscNode.send(tgt, m)
          }
        )
        timeOut --> Act(
          PrintLn("WARNING: /write has timed out"),
          done
        )
        (sendAll, done)
      }

      val chanLatch     = Var(0)

      val actPlay = Act(
        if verbose then PrintLn("-- play " /*++ busyPlay.toStr*/) else Act.Nop(),
        busyPlay  .set(true),
        shouldPlay.set(false),
        chanLatch .set(chanVr),
        rPlay.runWith(
          "bus" -> chanLatch,
        ),
      )

      val actRender = Act(
        if verbose then PrintLn("-- iteration " ++ iterCount.toStr ++ " select " ++ chanVr.toStr) else Act.Nop(),
        idle.set(false),
        dbAdvance.set((dbAdvance + dbAdvanceStep) % dbMaxAdvance),
        rSelect.runWith(
          "busy-rec"        -> busyRecVr,
          "busy-render"     -> busyRenderVr,
          "database"        -> db,
          "res-ovr-span"    -> resOvrSpan  ,
          "res-ovr-new-len" -> resOvrNewLen,
          "res-ins-pos"     -> resInsPos   ,
        ),
      )

      def mkRcvOverwrite(): Unit = {
        val rNextPhrase   = Runner("next-phrase")
        val rcvIterCount  = Var(0)
        val rcvCircleIdx  = Var(0)
        val rcvSpanStart  = Var(0L)
        val rcvSpanStop   = Var(0L)
        val rcvNewLen     = Var(0L)
        val rcvInsPos     = Var(0L)
        val rcvDb         = dbSeq.apply(rcvCircleIdx)
        val tr = oscNode.select("/write", rcvIterCount, rcvCircleIdx,
          rcvSpanStart, rcvSpanStop, rcvNewLen, rcvInsPos)
        val rcvAddr       = oscNode.sender.latch(tr)  // important to latch, because /relay may come in between
        tr --> Act(
          rNextPhrase.runWith(
            // "phrase-mod"  -> 2,
            "database"    -> rcvDb,
            "start"       -> rcvSpanStart,
            "stop"        -> rcvSpanStop,
            "new-length"  -> rcvNewLen,
            "match-pos"   -> rcvInsPos,
          )
        )
        val actReply = oscNode.send(rcvAddr, OscMessage("/written", rcvIterCount))
        rNextPhrase.done   --> actReply
        rNextPhrase.failed --> Act(
          PrintLn("next-phrase failed!"),
          actReply
        )
      }

      val actRenderDone = Act(
        if verbose then PrintLn("-- render done") else Act.Nop(),
        idle      .set(true),
        shouldPlay.set(true) // actPlay // If (awake) Then actPlay Else idle.set(true),
      )

      (shouldPlay && (!busyPlay/*.get*/)).toTrig --> actPlay

//      val condRun     = idle // /*&& next.get*/ && awake
      // val trRun       = condRun.toTrig
      // val actMaybeIterate  = If (condRun) Then actRender

      val actPlayDone = Act(
        if verbose then PrintLn("-- play done") else Act.Nop(),
        busyPlay.set(false),
//        idle    .set(true ),
//        actMaybeIterate,
      )

      val actNextRender = Act(
        chanVr.set(chanVr + 1),
        iterCount.set(iterCount + 1),
        If (chanVr < chansPerNode) Then actRender Else Act(
          if verbose then PrintLn("-- iteration done") else Act.Nop(),
//          self.done,
          chanVr.set(0),
//          actRender
          actRelay
        )
      )

      rPlay       .done     --> actPlayDone
      rPlay       .failed   --> Act(
        PrintLn("play-phrase failed!"),
        actPlayDone
      )
      rPlay       .running  --> actNextRender // actRender

      val (actSendOverwrite, doneSendOverwrite) = mkSendOverwrite()
      val actSelectDone = actSendOverwrite
      doneSendOverwrite --> actRenderDone
      mkRcvOverwrite()

      rSelect.done   --> actSelectDone
      rSelect.failed --> actRenderDone

      val (actHandshake, handshakeDone) = mkHandshake()
      val dbAdvanceInit = rng.range(0.0, dbMaxAdvance)
      val CircleSize = ChansPerNode * numNodes
      init --> Act(
        dbAdvanceInit.update,
        dbAdvance.set(dbAdvanceInit),
        if verbose then PrintLn("init. nodeIdx = " ++ nodeIdx.toStr ++ "; numNodes = " ++
          numNodes.toStr ++ "; dbAdvance = " ++ dbAdvance.toStr) else Act.Nop(),
        rReso.runWith(
//          "iter-dist" -> ((iterCount - circleOff) % CircleSize)
          "iter-mod"  -> (iterCount - circleOff) % CircleSize,
        ),
        rSoloBut.run,
        If (numNodes > 1) Then actHandshake Else actRender
      )
      // trRun --> actIterate
      handshakeDone --> Act(
        if verbose then PrintLn("Handshake done") else Act.Nop(),
//        rReso.run,
        If (nodeIdx sig_== 0) Then actRender
      )

      trRelay --> Act(
        if verbose then PrintLn("Relay received") else Act.Nop(),
        actRender
      )

      // debugging
//      (loadBang | next .changed) --> PrintLn("Writing - NEXT  IS " ++ next .toStr)
//      (loadBang | idle .changed) --> PrintLn("Writing - IDLE  IS " ++ idle .toStr)

    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}

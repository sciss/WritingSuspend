/*
 *  WritingSuspend.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.file.*
import de.sciss.log.{Level, Logger}
import de.sciss.lucre.{BooleanObj, DoubleObj, IntObj, Source, Workspace}
import de.sciss.lucre.synth.{InMemory, Server, Synth}
import de.sciss.proc.{AuralSystem, Control, FScape, Pattern, Runner, SoundProcesses, TimeRef, Universe}
import de.sciss.synth.{Client, SynthGraph, addAfter, addToTail, freeSelf}
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt}
import de.sciss.numbers.Implicits.*

import java.net.InetAddress
import scala.util.control.NonFatal

object WritingSuspend {
  type S = InMemory
  type T = InMemory.Txn

  final val SR = 48000 // Hz

  final val HighPassFreq = 80.0 // Hz

  final val ChansPerNode  = 8
  final val MaxNumNodes    = 3
//  final val NumNodes      = 3
//  final val CircleSize    = ChansPerNode * NumNodes
  final val MaxCircleSize    = ChansPerNode * MaxNumNodes

  def version : String = buildInfString("version")
  def builtAt : String = buildInfString("builtAtString")
  def name    : String = buildInfString("name")
  def fullName: String = s"$name v$version"

    /**
   * @param nodeId        logical id (1, 2, 3) of the node, or 0 for laptop
   * @param isLaptop      `true` to avoid having to use the GPIO
   * @param nodes         list of all nodes (logical ids) participating
   * @param dirAudio      base directory for static audio files (databases)
   * @param dirWork       base directory for dynamic content, i.e. RAM disk
   * @param oscBasePort   base port for inter-node OSC communication, the final
   *                      port will be this plus the nodeId. The reason for thus having
   *                      a different port per node is that it allows us to test-run
   *                      multiple nodes on the same computer.
   */
  case class Config(
                     nodeId         : Int       = -1,
                     verbose        : Boolean   = true,
                     isLaptop       : Boolean   = false,
                     nodes          : List[Int] = List(1, 2, 3),
                     debug          : Boolean   = false,
                     dirAudio       : File      = userHome / "Documents" / "projects" / "WritingSuspend" / "audio_work",
                     dirWork        : File      = file("/mnt/ramdisk"), // File.tempDir,
                     oscBasePort    : Int       = 0x5753, // 'WS'
                     jackClientName : String    = "Writing",
                     maxDbDur       : Double    = 102.0,
                     phrasePlayAmp  : Double    =  -6.0,
                     minPhraseDur   : Double    =   8.0,
                     maxPhraseDur   : Double    =  24.0,
                     minPhraseInsDur: Double    =   1.0,
                     maxPhraseInsDur: Double    =   6.0,
                     dbAdvanceStep  : Double    =  12.0,  // with 12 minutes per loop and 8 hours daily run, this wraps around
                     dbNames        : List[String] = List(
                       "db-arapaho.aif",
                       "db-arta.aif",
                       "db-cahuilla.aif",
                       "db-dhao.aif",
                       "db-enapa.aif",
                       "db-gbanzili.aif",
                       "db-gelao.aif",
                       "db-ixcatec.aif",
                       "db-jewishiraqi.aif",
                       "db-kholosi.aif",
                       "db-kujireray.aif",
                       "db-lopit.aif",
                       "db-manyjilyjarra.aif",
                       "db-ngaatjatjarra.aif",
                       "db-nivacle.aif",
                       "db-nuu.aif",
                       "db-pite.aif",
                       "db-saaroa.aif",
                       "db-shangaji.aif",
                       "db-surui.aif",
                       "db-taikhamyang.aif",
                       "db-tinigua.aif",
                       "db-zok.aif",
                       "db-zoque.aif",
                     ),
                     // XXX TODO:
                     dbGains        : List[Double] = List(
                       -4.5, 0.0,
                       0.0, 0.0,
                       -4.0 /* 0.0 */, -1.5 /* 0.0 */,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                       0.0, 0.0,
                     ),
                     resoGain       : Double    = -24.0,
                     resoFreqMin    : Double    = 0.05,
                     resoFreqMax    : Double    = 0.3,
                     resoPow        : Double    = 2.0,
                     resoClip       : Double    = 0.2,
                     mainGain       : Double    = 0.0,
                     // --- gpio ---
                     i2cBus         : Int       = 1,
                     // --- servo ---
                     servos         : Boolean   = true,
                     servoPwmMin    : Int       = 560,
                     servoPwmMax    : Int       = 2500,
                     servoFreq      : Double    = 50.0,
                     servoLag       : Double    = 0.99,
                     // --- solo ---
                     startMuted     : Boolean   = false,
                   ) extends Servo.Config

  private def buildInfString(key: String): String = try {
    val clazz = Class.forName("de.sciss.writing.BuildInfo")
    val m = clazz.getMethod(key)
    m.invoke(null).toString
  } catch {
    case NonFatal(_) => "?"
  }

  def printInfo(): Unit = {
    println(s"$fullName, built $builtAt")
  }

  val log: Logger = new Logger("writing")

  def thisIP(): String = {
    import sys.process._
    // cf. https://unix.stackexchange.com/questions/384135/
    val ifConfig = Seq("ip", "a", "show", "eth0").!!
    val ifConfigPat = "inet "
    val line = ifConfig.split("\n").map(_.trim).find(_.startsWith(ifConfigPat)).getOrElse("")
    val i0 = line.indexOf(ifConfigPat)
    val i1 = if i0 < 0 then 0 else i0 + ifConfigPat.length
    val i2 = line.indexOf("/", i1)
    if i0 < 0 || i2 < 0 then {
      val local = InetAddress.getLocalHost.getHostAddress
      Console.err.println(s"No assigned IP4 found in eth0! Falling back to $local")
      local
    } else {
      line.substring(i1, i2)
    }
  }

  private val dotToNodeId = Map.apply[Int, Int](
    36 -> 1,
    42 -> 2,
    30 -> 3,
    78 -> 0,
  )

  private val nodeIdToDot = dotToNodeId.map(_.swap)

  def resolveHost(nodeId: Int)(implicit config: Config): String =
    if nodeId == 0 && config.isLaptop then "127.0.0.1" else {
      val dot = nodeIdToDot(nodeId)
      s"192.168.77.$dot"
    }

  def resolveNodeId(ip: String): Int = {
    val dot0 = {
      val i = ip.lastIndexOf(".") + 1
      try {
        val last = ip.substring(i).toInt
        dotToNodeId(last)
      } catch {
        case NonFatal(_) => -1
      }
    }
    val res = if dot0 >= 0 then dot0 else 0
    if dot0 < 0 then println(s"Warning - could not determine 'dot' for IP $ip - assuming $res")
    res
  }

  def autoNodeId(): Int = {
    import sys.process._
    val nodeName = Seq("uname", "-n").!!.trim
    if nodeName == "aleph" then 0 else if nodeName.startsWith("raspberrypi") then {
      // nodeName.substring("klangpi".length).toInt % 32
      val ip = thisIP()
      resolveNodeId(ip)
    } else {
      Console.err.println(s"WARNING: Cannot parse node-name $nodeName. Falling back to node-id 0")
      0
    }
  }

  def shutdown(): Unit = {
    import sys.process._
    log.info("Issuing shutdown")
    val cmd = Seq("sudo", "shutdown", "now")
    cmd.!
  }

  def main(args: Array[String]): Unit = {
    printInfo()

    object p extends ScallopConf(args) {
      import org.rogach.scallop.*

      printedName = "WritingSuspend"
      private val default = Config()

      val debug: Opt[Boolean] = toggle(default = Some(default.debug),
        descrYes = "Debug mode.",
      )
      val isLaptop: Opt[Boolean] = toggle(default = Some(default.isLaptop),
        descrYes = "Run on laptop without GPIO and sensors.",
      )
      val nodeId: Opt[Int] = opt(
        descr = "Node-id or -1 (default) to automatically detect via 'uname'.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbosity on (default)",
        descrNo  = "Verbosity off",
      )
      val nodes: Opt[List[Int]] = opt(default = Some(default.nodes),
        descr = s"Differentiation minimum boost (default ${default.nodes.mkString(",")})"
      )
      val dirAudio: Opt[File] = opt(default = Some(default.dirAudio),
        descr = s"Audio file directory (default: ${default.dirAudio})"
      )
      val dirWork: Opt[File] = opt(default = Some(default.dirWork),
        descr = s"Base directory for working audio files (default: ${default.dirWork}).",
      )
      val oscBasePort: Opt[Int] = opt(default = Some(default.oscBasePort),
        descr = s"Base UDP port for inter-node communication. To this the node-ids will be added (default: ${default.oscBasePort}).",
      )
      val jackClientName: Opt[String] = opt(default = Some(default.jackClientName),
        descr = s"Jack Audio client name (default: ${default.jackClientName}).",
      )
      val maxDbDur: Opt[Double] = opt(
        default = Some(default.maxDbDur),
        descr = s"Maximum database search duration in seconds (default: ${default.maxDbDur}).",
        validate = x => x >= 10.0 && x <= 3600.0,
      )
      val phrasePlayAmp: Opt[Double] = opt(
        default = Some(default.phrasePlayAmp),
        descr = s"Phrase player gain in decibels (default: ${default.phrasePlayAmp}).",
        validate = x => x >= -360.0 && x <= 32.0,
      )
      val minPhraseDur: Opt[Double] = opt(
        default = Some(default.minPhraseDur),
        descr = s"Phrase minimum duration in seconds (default: ${default.minPhraseDur}).",
        validate = x => x >= 0.5 && x <= 100.0,
      )
      val maxPhraseDur: Opt[Double] = opt(
        default = Some(default.maxPhraseDur),
        descr = s"Phrase maximum duration in seconds (default: ${default.maxPhraseDur}).",
        validate = x => x >= 1.0 && x <= 360.0,
      )
      val minPhraseInsDur: Opt[Double] = opt(
        default = Some(default.minPhraseInsDur),
        descr = s"Phrase minimum insertion duration in seconds (default: ${default.minPhraseInsDur}).",
        validate = x => x >= 0.25 && x <= 30.0,
      )
      val maxPhraseInsDur: Opt[Double] = opt(
        default = Some(default.maxPhraseInsDur),
        descr = s"Phrase maximum insertion duration in seconds (default: ${default.maxPhraseInsDur}).",
        validate = x => x >= 0.5 && x <= 30.0,
      )
      val dbAdvanceStep: Opt[Double] = opt(
        default = Some(default.dbAdvanceStep),
        descr = s"Database window advancement per iteration in seconds (default: ${default.dbAdvanceStep}).",
        validate = x => x >= 0.0 && x <= 60.0,
      )
      val dbNames: Opt[List[String]] = opt(
        default = Some(default.dbNames),
        descr = s"File name per database entry (default: ${default.dbNames}).",
      )
      val dbGains: Opt[List[Double]] = opt(
        default = Some(default.dbGains),
        descr = s"Gain per database entry in decibels (default: ${default.dbGains}).",
      )
      val resoGain: Opt[Double] = opt(
        default = Some(default.resoGain),
        descr = s"Resonance gain in decibels (default: ${default.resoGain}).",
      )
      val resoFreqMin: Opt[Double] = opt(
        default = Some(default.resoFreqMin),
        descr = s"Resonance movement minimum frequency in Hz (default: ${default.resoFreqMin}).",
        validate = x => x > 0.0,
      )
      val resoFreqMax: Opt[Double] = opt(
        default = Some(default.resoFreqMax),
        descr = s"Resonance movement maximum frequency in Hz (default: ${default.resoFreqMax}).",
        validate = x => x > 0.0,
      )
      val resoPow: Opt[Double] = opt(
        default = Some(default.resoPow),
        descr = s"Resonance amplitude power factor (default: ${default.resoPow}).",
        validate = x => x > 0.0,
      )
      val resoClip: Opt[Double] = opt(
        default = Some(default.resoClip),
        descr = s"Resonance amplitude center clipping (default: ${default.resoClip}).",
        validate = x => x >= 0.0,
      )
      val mainGain: Opt[Double] = opt(
        default = Some(default.mainGain),
        descr = s"Main gain in decibels (default: ${default.mainGain}).",
      )     // --- gpio ---
      val i2cBus: Opt[Int] = opt(default = Some(default.i2cBus),
        descr = s"I2C bus id 0 to 17 (default: ${default.i2cBus}).",
        validate = x => x >= 0 && x <= 17
      )
      // --- servo ---
      val servos: Opt[Boolean] = toggle(default = Some(default.servos),
        descrYes = "Use servo motors (default)",
        descrNo = "Do not use servo motors",
      )
      val servoPwmMin: Opt[Int] = opt(
        default = Some(default.servoPwmMin),
        descr = s"Servo PWM minimum value in microseconds (default: ${default.servoPwmMin}).",
        validate = x => x >= 0 && x <= 5000,
      )
      val servoPwmMax: Opt[Int] = opt(
        default = Some(default.servoPwmMax),
        descr = s"Servo PWM maximum value in microseconds (default: ${default.servoPwmMax}).",
        validate = x => x >= 0 && x <= 5000,
      )
      val servoFreq: Opt[Double] = opt(
        default = Some(default.servoFreq),
        descr = s"Servo oscillator frequency in Hz (default: ${default.servoFreq}).",
        validate = x => x >= 0.0 && x <= 2000.0,
      )
      val servoLag: Opt[Double] = opt(
        default = Some(default.servoLag),
        descr = s"Servo movement lagging coefficient (default: ${default.servoLag}).",
        validate = x => x >= 0.0 && x <= 1.0,
      )
      // --- solo ---
      val startMuted: Opt[Boolean] = toggle(default = Some(default.startMuted),
        descrYes = "Started in muted state",
        descrNo = "Do not start in muted state (default)",
      )
      verify()

      private val nodeIdValue = nodeId.getOrElse(autoNodeId())

      val config: Config = Config(
        nodeId          = nodeIdValue,
        verbose         = verbose(),
        isLaptop        = isLaptop(),
        nodes           = nodes(),
        debug           = debug(),
        dirAudio        = dirAudio(),
        dirWork         = dirWork(),
        oscBasePort     = oscBasePort(),
        jackClientName  = jackClientName(),
        maxDbDur        = maxDbDur(),
        phrasePlayAmp   = phrasePlayAmp(),
        minPhraseDur    = minPhraseDur   (),
        maxPhraseDur    = maxPhraseDur   (),
        minPhraseInsDur = minPhraseInsDur(),
        maxPhraseInsDur = maxPhraseInsDur(),
        dbAdvanceStep   = dbAdvanceStep  (),
        dbNames         = dbNames(),
        dbGains         = dbGains(),
        resoGain        = resoGain(),
        resoFreqMin     = resoFreqMin(),
        resoFreqMax     = resoFreqMax(),
        resoPow         = resoPow (),
        resoClip        = resoClip(),
        mainGain        = mainGain(),
          // --- gpio ---
        i2cBus          = i2cBus(),
        // --- servo ---
        servos          = servos(),
        servoPwmMin     = servoPwmMin(),
        servoPwmMax     = servoPwmMax(),
        servoFreq       = servoFreq(),
        servoLag        = servoLag(),
        // --- solo ---
        startMuted      = startMuted(),
      )
    }

    implicit val c: Config = p.config

    if (c.verbose) log.level = Level.Info

    log.info(s"Node-id is ${c.nodeId}")
    log.info(s"GPIO Mem? ${sys.env.getOrElse("WIRINGPI_GPIOMEM", "undefined")}")

    init()

    if !c.isLaptop then {
      // disable screen blanking
      import sys.process._
      Seq("xset", "s", "off").!
      Seq("xset", "-dpms").!
    }

    implicit val system: S = mkSystem()

//    implicit val rnd: Random = new Random()
    run()
  }

  def run()(implicit config: Config, system: S): Unit = {
    val (ctlH, chanRefH, swerveRefH, muteRefH): (Source[T, Control[T]], Source[T, IntObj.Var[T]], Source[T, DoubleObj.Var[T]], Source[T, BooleanObj.Var[T]]) =
      system.step /*SoundProcesses.step[T]("init")*/ { implicit tx =>

      val circleOff         = math.max(0, config.nodeId - 1) * ChansPerNode
      val busyPlayRef       = BooleanObj.newVar[T](false)
      val chanRef           = IntObj    .newVar[T](0)
      val progressSwerveRef = DoubleObj .newVar[T](0.5)
      val muteRef           = BooleanObj.newVar[T](config.startMuted)
      val ctl               = Build(circleOff = circleOff, busyPlayRef = busyPlayRef,
        chanRef = chanRef, progressSwerveRef = progressSwerveRef, muteRef = muteRef)

      if config.servos && !config.isLaptop then Servo(chanRef = chanRef, swerveRef = progressSwerveRef)

      (tx.newHandle(ctl), tx.newHandle(chanRef), tx.newHandle(progressSwerveRef), tx.newHandle(muteRef))
    }

    bootWith(muteRefH) { implicit tx => implicit u => _ /*s*/ =>
      val ctl = ctlH()
      val r   = Runner[T](ctl)
      r.react { implicit tx => {
        case Runner.Done =>
          val chanRef   = chanRefH()
          val swerveRef = swerveRefH()
          for ch <- 0 until ChansPerNode do {
            chanRef()   = ch
            swerveRef() = 0.51    // need to make sure it changes
            swerveRef() = 0.50
          }

          // give the servos two seconds to return to center
          // ; and play audible signal for that time
          log.info("Beginning shutdown")
          u.auralSystem.serverOption.foreach { s =>
            try {
              val gMain = SynthGraph {
                import de.sciss.synth.ugen.{VDiskIn => _, *}
                import de.sciss.synth.Import.*
                import de.sciss.synth.Ops.stringToControl // !! we're passing directly here

                val amp     = "amp".kr(0.1)
                val env     = EnvGen.ar(Env.linen(attack = 0.01, release = 1.5), levelScale = amp, doneAction = freeSelf)
                val freq    = "freq".kr(440)
                val sigIn   = SinOsc.ar(freq)
                val sigOut  = sigIn * env
                ReplaceOut.ar(0, sigOut) // hpf, limiter?
              }
              val freq = config.nodeId.clip(0, 3) * 100 + 400
              Synth.play(gMain, Some("main"))(s.defaultGroup,
                args = Seq("amp" -> config.resoGain.dbAmp, "freq" -> freq),
                addAction = addToTail)
            } catch {
              case NonFatal(ex) =>
                println("Huh?")
                ex.printStackTrace()
            }
          }

          val sch = u.scheduler
          sch.schedule(sch.time + (TimeRef.SampleRate * 2).toLong) { implicit tx =>
            tx.afterCommit {
              if config.isLaptop then sys.exit() else WritingSuspend.shutdown()
            }
          }

        case _ => ()
      }}
      r.run()
    }
  }

  def init(): Unit = {
    SoundProcesses.init()
    FScape        .init()
    Pattern       .init()
  }

  def mkSystem(): S = InMemory()

  def bootWith(muteRefH: Source[T, BooleanObj[T]])(done: T => Universe[T] => Server => Unit)
              (implicit system: InMemory, config: Config): Unit = {
    val as    = AuralSystem(global = true)
    val sCfg  = Server.Config()
    sCfg.inputBusChannels   = 0
    sCfg.outputBusChannels  = ChansPerNode
    sCfg.deviceName         = Some(config.jackClientName)
    sCfg.pickPort()
    val cCfg                = Client.Config()
    system.step { implicit tx =>
      as.react { tx => {
        case AuralSystem.Running(s) =>
          tx.afterCommit {
            // s.peer.dumpOSC()
            SoundProcesses.step[T]("booted") { implicit tx =>
              implicit val ws: Workspace[T] = Workspace.Implicits.dummy[T]
              implicit val u: Universe[T] = Universe[T]()

              if true /*config.mainGain != 0.0*/ then {
                log.info("Starting main synth")
                try {
                  val gMain = SynthGraph {
                    import de.sciss.synth.ugen.{VDiskIn => _, *}
                    //              import de.sciss.synth.proc.graph.*
                    import de.sciss.synth.Curve
                    import de.sciss.synth.Import.*
                    import de.sciss.synth.Ops.stringToControl // !! we're passing directly here

                    val amp = "main-amp".kr(1.0)
                    val sigIn = In.ar(0, ChansPerNode)
                    val sigOut = sigIn * amp
                    ReplaceOut.ar(0, sigOut) // hpf, limiter?
                  }
                  val muteRef     = muteRefH()
                  def mkAmp(mute: Boolean): Double = if mute then 0.0 else config.mainGain.dbAmp
                  val syn = Synth.play(gMain, Some("main"))(s.defaultGroup,
                    args = Seq("main-amp" -> mkAmp(muteRef.value)), addAction = addAfter)
                  muteRef.changed.react { implicit tx => ch => {
                    syn.set("main-amp" -> mkAmp(ch.now))
                  }}

                } catch {
                  case NonFatal(ex) =>
                    println("Huh?")
                    ex.printStackTrace()
                }
              }

              done(tx)(u)(s)
            }
          }

        case _ => ()
      }}
      as.start(sCfg, cCfg)
    }
  }
}

/*
 *  Resonance.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.proc.{Control, Proc}
import de.sciss.synth.{GE, SynthGraph}
import de.sciss.writing.WritingSuspend.{ChansPerNode, /*CircleSize,*/ T}

object Resonance {
  def apply()(implicit tx: T): Proc[T] = {
    val g = SynthGraph {
      import de.sciss.synth.ugen.{VDiskIn => _, PartConv => _, *}
      import de.sciss.synth.proc.graph.*
      import de.sciss.synth.Import.*
      import de.sciss.synth.proc.graph.Ops.*

//      val phraseChan = "chan".kr
      val fftSize   = 2048
      val fuzzyAmp  = 2.0 / fftSize
      val amp       = "amp" .kr(1.0)
//      amp.poll(0, "resonance-amp")
      val noiseFreqMin = "freq-min" .kr(0.1)
      val noiseFreqMax = "freq-max" .kr(0.2)
      val noisePow  = "pow"  .kr(4.0)
      val noiseClip = "clip" .kr(0.1)
      val sigIn     = WhiteNoise.ar(Seq.fill(ChansPerNode)(amp))
      val fuzzy: GE = Seq.tabulate(ChansPerNode) { ch =>
        PartConv.ar(s"reso-${ch + 1}", sigIn.out(ch), fftSize = fftSize) * fuzzyAmp
      }
      val noiseFreq = Seq.fill(ChansPerNode)(LFNoise0.kr(1.0).linLin(-1, 1, noiseFreqMin, noiseFreqMax))
      val movement  = LFNoise2.kr(noiseFreq).abs.pow(noisePow).excess(noiseClip) // / (1 - noiseClip)
//      movement.out(0).poll(1, "movement")

      val iterMod  = "iter-mod" .kr(0)
      val numNodes = "num-nodes".kr(3)
      val minChanBetween = numNodes * 0.5 + 2
      val CircleSize = ChansPerNode * numNodes
      val angFactor  = math.Pi * 2 / CircleSize
      val chanFactor = angFactor.reciprocal
      val iterAng = iterMod * angFactor
      val gate: GE = Seq.tabulate(ChansPerNode) { ch =>
        val chanAng = ch * angFactor
        val x = iterAng
        val y = chanAng
        val angBetween  = (x-y).sin atan2 (x-y).cos
        val chanBetween = angBetween.abs * chanFactor
        chanBetween > minChanBetween
      }

//      val gate      = Seq.tabulate(ChansPerNode) { ch =>
//        (phraseChan absDif ch) > 1.5
//      }

      val gateL     = Lag.kr(gate, 4.0)
      val sig       = fuzzy * movement * gateL

//      sig.poll(1, "RESO-SIG")

      PhysicalOut.ar(0, sig)
    }
    val p = Proc[T]()
    p.graph() = g
    p
  }
}

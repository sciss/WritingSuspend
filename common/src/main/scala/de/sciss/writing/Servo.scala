/*
 *  Servo.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import com.pi4j.io.gpio.{GpioFactory, Pin}
import com.pi4j.io.i2c.I2CFactory
import de.sciss.lucre.{Disposable, DoubleObj, IntObj}
import de.sciss.numbers.Implicits.doubleNumberWrapper
import de.sciss.writing.WritingSuspend.{ChansPerNode, T}
import pi4j.component.servo.impl.PCA9685GpioServoProvider
import pi4j.gpio.extension.pca.{PCA9685GpioProvider, PCA9685Pin}

import java.util.{Timer, TimerTask}

object Servo {
  trait Config {
    def i2cBus     : Int
    def servoPwmMin: Int
    def servoPwmMax: Int
    def servoFreq  : Double
    def servoLag   : Double
  }

  // the returned `Disposable` is for shutdown
  def apply(chanRef: IntObj[T], swerveRef: DoubleObj[T])(implicit tx: T, config: Config): Disposable[T] = {
    val pwmZero     = (config.servoPwmMin + config.servoPwmMax) / 2
    val swerveTgtA  = Array.fill(ChansPerNode)(0.5f)
    val microsCurrA = Array.fill(ChansPerNode)(pwmZero)
    val stillCntA   = Array.fill(ChansPerNode)(0)
    val swerveCurrA = Array.fill(ChansPerNode)(0.5f)

    swerveRef.changed.react { implicit tx => ch =>
      val chan    = chanRef.value
      val swerve  = ch.now
//      tx.afterCommit {
//        println(s"chan $chan new raw value $swerve")
//      }
      if chan >= 0 && chan <= ChansPerNode then {
        // we don't need to bother about tx commit, just put the value
        swerveTgtA(chan) = swerve.toFloat
      }
    }

    tx.afterCommit {
      val servoProvider = createProvider()
      val timer   = new Timer("servos", true)
      val timerFreq = 50  // Hz
      val period  = 1000 / timerFreq
      timer.scheduleAtFixedRate(new TimerTask {
        private final val w1 = config.servoLag.toFloat
        private final val w2 = 1.0f - w1
        private final val maxAng = 4.0f /180  // linear speed limiter

        override def run(): Unit = {
          var chan = 0
          while chan < ChansPerNode do {
            val vOld    = swerveCurrA(chan)
            val vTgt    = swerveTgtA (chan)
            // println(s"chan $chan sees curr $vOld and target $vTgt")
            val vNew0   = vOld * w1 + vTgt * w2
            val vNew    =
              if      vNew0 > vOld + maxAng then vOld + maxAng
              else if vNew0 < vOld - maxAng then vOld - maxAng
              else                               vNew0
            swerveCurrA(chan) = vNew
            val mOld  = microsCurrA(chan)
            val mNew1 = Servo.calculatePwmDuration(vNew, lo = 0.0, hi = 1.0)
            val pinIdx1 = chan << 1
            val pinIdx2 = pinIdx1 + 1
            if mNew1 != mOld then {
              // println(s"chan $chan new micros $mNew1")
//              // symmetric
//              val mNew2 = Servo.calculatePwmDuration(1.0 - vNew, lo = 0.0, hi = 1.0)
              val mNew2 = mNew1
              microsCurrA(chan) = mNew1
              stillCntA(chan) = 0
              val pin1        = pins(pinIdx1)
              val pin2        = pins(pinIdx2)
              val driver1     = servoProvider.getServoDriver(pin1)
              val driver2     = servoProvider.getServoDriver(pin2)
              driver1.setServoPulseWidth(mNew1)
              driver2.setServoPulseWidth(mNew2)
            } else {
              val stillNew = stillCntA(chan) + 1
              if stillNew == 20 then {
                // after a delay, turn the servo off
                val pin1 = pins(pinIdx1)
                val pin2 = pins(pinIdx2)
                val driver1 = servoProvider.getServoDriver(pin1)
                val driver2 = servoProvider.getServoDriver(pin2)
                driver1.getProvider.setAlwaysOff(pin1)
                driver2.getProvider.setAlwaysOff(pin2)
              } else {
                stillCntA(chan) = stillNew
              }
            }
            chan += 1
          }
        }
      }, period, period)
    }

    Disposable.empty[T] // XXX TODO
  }

  def calculatePwmDuration(angle: Double, lo: Double = 0.0, hi: Double = 180.0)(implicit config: Config): Int =
    (angle.clip(lo, hi).linLin(lo, hi, config.servoPwmMin, config.servoPwmMax) + 0.5).toInt

  def createProvider()(implicit config: Config): PCA9685GpioServoProvider /*PCA9685GpioProvider*/ = {
    val bus = I2CFactory.getInstance(config.i2cBus)
    val gpioProvider = new PCA9685GpioProvider(bus, 0x40, new java.math.BigDecimal(config.servoFreq))
    val gpio = GpioFactory.getInstance
    Servo.pins.foreach { pin =>
      /* val pin = */ gpio.provisionPwmOutputPin(gpioProvider, pin)
    }

    new PCA9685GpioServoProvider(gpioProvider)
  }

  val pins: Seq[Pin] = Array(
    PCA9685Pin.PWM_00,
    PCA9685Pin.PWM_01,
    PCA9685Pin.PWM_02,
    PCA9685Pin.PWM_03,
    PCA9685Pin.PWM_04,
    PCA9685Pin.PWM_05,
    PCA9685Pin.PWM_06,
    PCA9685Pin.PWM_07,
    PCA9685Pin.PWM_08,
    PCA9685Pin.PWM_09,
    PCA9685Pin.PWM_10,
    PCA9685Pin.PWM_11,
    PCA9685Pin.PWM_12,
    PCA9685Pin.PWM_13,
    PCA9685Pin.PWM_14,
    PCA9685Pin.PWM_15,
  ).toIndexedSeq
}

/*
 *  Select.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.proc.Control
import de.sciss.writing.WritingSuspend.{Config, T}

object Select {
  def apply()(implicit tx: T, config: Config): Control[T]= {
    import config.{verbose, debug}
    val g = Control.Graph {
      import de.sciss.proc.ExImport.*
      import de.sciss.lucre.expr.graph.*

      val self          = ThisRunner()
      val rOvrSelect    = Runner("overwrite-select")
      val rMatchSelect  = Runner("match-select")
      val progressOvr   = Var(0.0)
      val progressMatch = rMatchSelect.progress.max(0.0)  // N.B. unknown progress indicated by -1

      // cf. Lucre issue #70
      val resOvrSpan    = "res-ovr-span"   .attr(Span(0L, 0L))
      val resOvrNewLen  = "res-ovr-new-len".attr(0L)
      val resInsPos     = "res-ins-pos"    .attr(0L)
      val resOvrSpanVr  = Var[Span]()
      val resOvrNewLenVr= Var[Long]()
      val resInsPosVr   = Var[Long]()

      val dbCue         = "database".attr[AudioCue](AudioCue.Empty())
      val phCueIn       = "phrase"  .attr[AudioCue](AudioCue.Empty())
      val busyRec       = self.attr[Boolean]("busy-rec")
      val busyRender    = self.attr[Boolean]("busy-render")
      val busyRecVr     = Var(false); busyRecVr     --> busyRec    // cf. Lucre issue #70
      val doneMatch     = rMatchSelect.done

      Seq(
        rOvrSelect    -> "overwrite-select",
        rMatchSelect  -> "match-select",
      ).foreach { case (runner, key) =>
        val failure = runner.messages.mkString(s"FAILURE: $key :\n", "\n", "")
        runner.failed --> Act(
          PrintLn(failure),
          self.fail(failure)
        )
      }

      val timeOut = Delay(60.0) // somehow the rendering can hang
      val init    = LoadBang()

      init --> Act(
        timeOut,
        busyRender.set(true),
        rOvrSelect.runWith(
          "span"        -> resOvrSpanVr,
          "new-length"  -> resOvrNewLenVr,
          "progress"    -> progressOvr,
        )
      )

      rOvrSelect.done --> Act(
        resOvrSpan  .set(resOvrSpanVr   ),
        resOvrNewLen.set(resOvrNewLenVr ),
        if debug then PrintLn(
          "writing instr: "  ++ resOvrSpan  .toStr ++
            ", newLength = " ++ resOvrNewLen.toStr
        ) else Act.Nop(),
        rMatchSelect.runWith(
          "database"    -> dbCue,
          "phrase"      -> phCueIn,
          "start"       -> resOvrSpanVr.start,
          "stop"        -> resOvrSpanVr.stop,
          "new-length"  -> resOvrNewLenVr,
          "result"      -> resInsPosVr,
        )
      )

      val actDone: Act = Act(
        busyRender.set(false),
        self.done,
      )

      doneMatch --> Act(
        resInsPos.set(resInsPosVr),
        if debug then PrintLn("writing match start: " ++ resInsPos.toStr) else Act.Nop(),
        actDone,
      )

      timeOut --> Act(
        if verbose then PrintLn("WARN: writing select time-out") else Act.Nop(),
        actDone,
      )

      val progressSwerve = (1.0 - progressOvr) * 0.5 +
        (progressMatch * 1.5).fold(0.0, 1.0) // -1 to + 1
      val progressRes = "progress-swerve".attr(0.0)
      progressSwerve --> progressRes

//      init --> PrintLn("INIT progressOvr " ++ progressOvr.toStr ++ " ; progressMatch " ++ progressMatch.toStr)

//      progressOvr   .changed --> PrintLn("PROGRESS O: " ++ progressOvr  .toStr)
//      progressMatch .changed --> PrintLn("PROGRESS M: " ++ progressMatch.toStr)
    }
    val c = Control[T]()
    c.graph() = g
    c
  }
}

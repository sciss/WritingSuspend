/*
 *  Build.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.audiofile.{AudioFile, AudioFileSpec, AudioFileType, SampleFormat}
import de.sciss.file.*
import de.sciss.lucre.{Artifact, ArtifactLocation, BooleanObj, DoubleObj, Folder, IntObj, LongObj, StringObj}
import de.sciss.proc.{AudioCue, Control}
import de.sciss.writing.WritingSuspend.{ChansPerNode, Config, MaxCircleSize, SR, T, log, resolveHost}
import de.sciss.numbers.Implicits.doubleNumberWrapper

object Build {
  def apply(circleOff: Int, busyPlayRef: BooleanObj[T], chanRef: IntObj[T],
            progressSwerveRef: DoubleObj[T], muteRef: BooleanObj[T])
           (implicit tx: T, config: Config): Control[T] = {
    // ---- files ----
    val dbDirT  = config.dirAudio / "db"
    val phDirT  = config.dirWork  / "ph"
    val aPh0    = phDirT / "ph0.aif"
    val numNodes = config.nodes.size

    phDirT.mkdirs()

    AudioFile.openWrite(aPh0, AudioFileSpec(
      AudioFileType.AIFF,
      sampleFormat  = SampleFormat.Float,
      sampleRate    = SR,
      numChannels   = 2,
    )).close()

    val locDB   = ArtifactLocation.newConst[T](dbDirT.toURI)
    val locPh   = ArtifactLocation.newConst[T](phDirT.toURI)

    def mkCue(loc: ArtifactLocation[T], f: File, amp: Double): AudioCue.Obj[T] = {
      val uri   = f.toURI
      val a     = Artifact[T](loc, uri)
      val spec  = AudioFile.readSpec(f)
//      val cue   = AudioCue(uri, spec, 0L, 1.0)
      AudioCue.Obj[T](a, spec, offset = LongObj.newConst(0L), gain = DoubleObj.newConst(amp))
//      AudioCue.Obj.newConst[T](cue)
    }

    // ---- objects ----
    val iPhCount        = IntObj.newVar[T](0)
    val cuePhrase       = AudioCue.Obj.newVar[T](mkCue(locPh, aPh0, amp = 1.0))

//    val CircleSize = ChansPerNode * numNodes
    val cueDBSeq  = Seq.tabulate(MaxCircleSize) { circleIdx =>
//      val dbName = dbDirT / s"db$circleIdx.aif"
      val dbName        = config.dbNames(circleIdx)
      val fDb           = dbDirT / dbName
      val dbAmp         = config.dbGains(circleIdx).dbAmp
      val cueDbV        = mkCue(locDB, fDb, amp = dbAmp)
      log.info(s"db [$circleIdx] = ${cueDbV.value}")
      val cueDb         = AudioCue.Obj.newVar[T](cueDbV)
      // val aCue          = cueDb.attr
      // aCue.put("amp", DoubleObj.newConst[T])(dbAmp))
      cueDb
    }
    val dbFolder = Folder[T]()
    cueDBSeq.foreach(dbFolder.addLast)

    val cueResoSeq = Seq.tabulate(ChansPerNode) { ch =>
      val circleIdx = circleOff + ch
      val dbName    = config.dbNames(circleIdx)
      val resoName  = s"${dbName.substring(0, dbName.indexOf("."))}-reso.aif"
      val fReso     = dbDirT / resoName
      val cueResoV  = mkCue(locDB, fReso, amp = 1.0)
      val cueReso   = cueResoV // AudioCue.Obj.newVar[T](cueResoV)
      cueReso
    }

    val fMatchSelect      = MatchSelect()
    val aMatchSelect      = fMatchSelect.attr
    val vMaxDbDur         = DoubleObj.newConst[T](config.maxDbDur)
    aMatchSelect.put("max-db-dur", vMaxDbDur)
    val cNextPhrase       = NextPhrase()
    val aNextPhrase       = cNextPhrase.attr
    aNextPhrase.put("ph-count", iPhCount  )
    aNextPhrase.put("phrase"  , cuePhrase )
    val cOverwriteSelect  = OverwriteSelect()
    val aOverwriteSelect  = cOverwriteSelect.attr
    aOverwriteSelect.put("phrase", cuePhrase)
    aOverwriteSelect.put("min-phrase-dur"     , DoubleObj.newConst[T](config.minPhraseDur   ))
    aOverwriteSelect.put("min-phrase-ins-dur" , DoubleObj.newConst[T](config.minPhraseInsDur))
    aOverwriteSelect.put("max-phrase-dur"     , DoubleObj.newConst[T](config.maxPhraseDur   ))
    aOverwriteSelect.put("max-phrase-ins-dur" , DoubleObj.newConst[T](config.maxPhraseInsDur))
    val cSelect           = Select()
    val aSelect           = cSelect.attr
    // aRender.put("database"          , cueDatabase)
    aSelect.put("match-select"      , fMatchSelect)
//    aSelect.put("overwrite-perform" , fOverwritePerform)
    aSelect.put("overwrite-select"  , cOverwriteSelect)
    aSelect.put("phrase"            , cuePhrase)
    aSelect.put("progress-swerve"   , progressSwerveRef)
    val cPlayPhrase = PlayPhrase(amp = config.phrasePlayAmp.dbAmp /*, limLevel = playLimLevel*/)
    val aPlayPhrase = cPlayPhrase.attr
    aPlayPhrase.put("phrase", cuePhrase)

    val cReso = Resonance()
    val aReso = cReso.attr
    aReso.put("amp"     , DoubleObj.newConst[T](config.resoGain.dbAmp ))
    aReso.put("freq-min", DoubleObj.newConst[T](config.resoFreqMin    ))
    aReso.put("freq-max", DoubleObj.newConst[T](config.resoFreqMax    ))
    aReso.put("pow"     , DoubleObj.newConst[T](config.resoPow        ))
    aReso.put("clip"    , DoubleObj.newConst[T](config.resoClip       ))
    aReso.put("num-nodes", IntObj.newConst[T](numNodes))
    aReso.put("chan"    , chanRef)
    cueResoSeq.zipWithIndex.foreach { case (cueReso, ch) =>
      aReso.put(s"reso-${ch + 1}", cueReso)
    }

    val fPorts = Folder[T]()
    val fHosts = Folder[T]()
    for nodeId <- config.nodes do {
      val host = if config.isLaptop || numNodes == 1 then "127.0.0.1" else resolveHost(nodeId)
      val port = config.oscBasePort + nodeId
      fPorts.addLast(IntObj   .newConst[T](port))
      fHosts.addLast(StringObj.newConst[T](host))
    }

    val nodeIdx = math.max(0, config.nodeId - 1)
    
    val butCtl  = SoloButtonControl()
    val aButCtl = butCtl.attr
    aButCtl.put("mute", muteRef)

    val cIter = Iteration()  // aka 'Control' in "Writing (simultan)"
    val aIter = cIter.attr
//    aIter.put("database"    , cueDatabase)
    aIter.put("db-folder"         , dbFolder)
    aIter.put("circle-off"        , IntObj.newConst[T](circleOff))
    aIter.put("ph-count"          , iPhCount)
    aIter.put("phrase"            , cuePhrase)
    aIter.put("max-db-dur"        , vMaxDbDur)
    aIter.put("db-advance-step"   , DoubleObj.newConst[T](config.dbAdvanceStep))
    aIter.put("play-phrase"       , cPlayPhrase)
    aIter.put("select"            , cSelect)
    aIter.put("next-phrase"       , cNextPhrase)
    aIter.put("busy-play"         , busyPlayRef)
    aIter.put("chan"              , chanRef)
    aIter.put("chans-per-node"    , IntObj.newConst[T](ChansPerNode))
    aIter.put("port-folder"       , fPorts)
    aIter.put("host-folder"       , fHosts)
    aIter.put("node-idx"          , IntObj.newConst[T](nodeIdx))
    aIter.put("resonance"         , cReso)
    aIter.put("solo-buttons"      , butCtl)
    cIter
  }
}

/*
 *  NextPhrase.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.fscape.{GE, Graph}
import de.sciss.proc.{Control, FScape}
import de.sciss.writing.WritingSuspend.{Config, SR, T}

/*

  Applies the phase replacement,
  and sets (returns) the new phase cue and phase count

 */
object NextPhrase {
  def apply()(implicit tx: T, config: Config): Control[T] = {
    val c   = mkControl()
    val f   = mkFScape()
    val a   = c.attr
    a.put("fsc", f)
    c
  }

  private def mkControl()(implicit tx: T, config: Config): Control[T] = {
    import config.{verbose, debug}
    val g = Control.Graph {
      import de.sciss.lucre.expr.graph.*
      import de.sciss.proc.ExImport.*

      val self  = ThisRunner()
      val rFSc  = Runner("fsc")

      val instrSpanStart  = "start"     .attr(0L)
      val instrSpanStop   = "stop"      .attr(0L)
      val instrNewLen     = "new-length".attr(0L)
      val matchPos        = "match-pos" .attr(0L)
      val phraseMod       = "phrase-mod".attr(2)

      val dbCue           = "database"  .attr[AudioCue](AudioCue.Empty())
      val phCueIn         = "phrase"    .attr[AudioCue](AudioCue.Empty())
      val dbAmp           = dbCue.gain

      val init            = LoadBang()
      val dbFile          = dbCue.artifact
      val phFileIn        = phCueIn.artifact
      val phCount         = "ph-count".attr(0)
      val phCount1        = (phCount.latch(init) + 1) % phraseMod
      val phFileOut       = phFileIn.replaceName("ph" ++ phCount1.toStr ++ ".aif")
      val specPhOut       = AudioFileSpec.read(phFileOut).getOrElse(AudioFileSpec.Empty())
      val phCueOut        = AudioCue(phFileOut, specPhOut)

      val actRun = rFSc.runWith(
        "start"       -> instrSpanStart,
        "stop"        -> instrSpanStop ,
        "new-length"  -> instrNewLen   ,
        "match-pos"   -> matchPos      ,
        "db"          -> dbFile,
        "db-amp"      -> dbAmp,
        "in-ph"       -> phFileIn,
        "out-ph"      -> phFileOut,
      )

      val actDone = Act(
        if verbose then PrintLn("next phrase done") else Act.Nop(),
        if debug then PrintLn(dbCue.toStr) else Act.Nop(),
        phCount.set(phCount1),
        phCueIn.set(phCueOut),
        if debug then PrintLn(phCueIn.toStr) else Act.Nop(),
        self.done,
      )

      rFSc.stoppedOrDone --> actDone
      rFSc.failed --> Act(
        PrintLn("next-phrase failed: " ++ rFSc.messages.mkString("; ")),
        self.fail("next-phrase failed")
      )

      init --> Act(
//        if debug then PrintLn("next-phrase start " ++ instrSpanStart.toStr ++ "; stop " ++
//          instrSpanStop.toStr ++ "; new-length " ++ instrNewLen.toStr ++ "; match-pos " ++ matchPos.toStr ++
//          "; db-file " ++ dbFile.toStr ++ "; in-ph " ++ phFileIn.toStr ++ "; out-ph " ++ phFileOut.toStr) else Act.Nop(),
        actRun
      )
    }
    val c = Control[T]()
    c.graph() = g
    c
  }

  private def mkFScape()(implicit tx: T): FScape[T] = {
    val g = Graph {
      import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, *}
      import de.sciss.fscape.lucre.graph.*
      import de.sciss.fscape.Ops.*
      import de.sciss.fscape.lucre.graph.Ops.*

      // XXX TODO: `def`s here because of deadlock between
      // If predicate and branch use
      def instrSpanStart  = "start"     .attr[Long]
      def instrSpanStop   = "stop"      .attr[Long]
      def instrNewLen     = "new-length".attr[Long]
      val matchPos        = "match-pos" .attr[Long]

      val WitheringConstant = 0.0078125 // = 1.0/128
      val SRd = SR.toDouble // "sample-rate".attr(44100.0) // 48000.0

      val phPos = matchPos        // instrSpanStart
      val dbPos = instrSpanStart  // matchPos

      // XXX TODO: `def`s here: see above
      def spliceLen   = instrNewLen // min(dbSpan.length, instrNewLen)
      def insSpanLen  = instrSpanStop - instrSpanStart
      def mkInsFdLen  = (insSpanLen / 2) min (spliceLen / 2) min (SRd * 0.1).toInt // .floor

      val db    = AudioFileIn("db") // db0.f, numChannels = 1)
      val dbAmp = "db-amp".attr[Double]
      dbAmp.poll("dbAmp")
      val dbW   = db * (Seq[GE.D](dbAmp, 0.0): GE.D)
      val inPh0 = AudioFileIn("in-ph") // ph0.f, numChannels = 2)
      val inPh  = inPh0 + (Seq[GE.D](0.0, WitheringConstant): GE.D) // withering

      val insPre  = inPh.take(phPos)
      val insPost = inPh.drop(instrSpanStop)
      val insMid  = dbW.drop(dbPos).take(spliceLen)

      // mkInsFdLen.poll("insFdLen")

      val insCat = If (mkInsFdLen sig_== 0) Then {
        insPre ++ insMid ++ insPost

      } Else {
        val insFdLen = mkInsFdLen
        // we could use .sqrt for left channel and linear for right channel;
        // for simplicity, just use linear for both.
        // NOTE: we do not fade in/out the withering channel, because that
        // would build up stuff at the phrase beginning and end!
        val preOut   = inPh.drop(phPos).take(insFdLen) * Line(Seq[GE.D](1.0, 0.0), 0.0, insFdLen) // .sqrt
        val midIn   = insMid.take(insFdLen) * Line(Seq[GE.D](0.0, 1.0), 1.0, insFdLen) // .sqrt
        val cross0  = preOut + midIn
        val takeLen: GE.L = spliceLen - (insFdLen * 2)
        val cross1  = insMid.drop(insFdLen).take(takeLen)
        val midOut  = insMid.drop(spliceLen - insFdLen) * Line(1.0, Seq[GE.D](0.0, 1.0), insFdLen) // .sqrt
        val postIn  = inPh.drop(instrSpanStop - insFdLen).take(insFdLen) * Line(0.0, Seq[GE.D](1.0, 0.0), insFdLen) // .sqrt
        val cross2  = midOut + postIn
        val cross   = cross0 ++ cross1 ++ cross2

        insPre ++ cross ++ insPost
      }
      AudioFileOut("out-ph" /*file = ph1F*/ ,
        in = insCat.clip2(1.0),
        sampleRate = SRd,
        sampleFormat = 2,
      )
    }
    val f = FScape[T]()
    f.graph() = g
    f
  }
}

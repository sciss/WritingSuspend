// Regular OpenSCAD Variables
$fn = 120; // 180;// Quality

petriInnerDiam      = 89.0;
piezoDiam           = 40.0;
piezoTol            =  1.0;
piezoTolDiam        = piezoDiam + 2*piezoTol;
piezoOverhangAmt    = 3.0;
piezoOverhangDiam   = piezoDiam - 2 * piezoOverhangAmt;
wallThick           = 2.5;  // ist falsch; die dicke ist halb so gross wie dieser wert!
baseThick           = 2.0;
holderHeight        = 1.0 + baseThick; // 6
armThick            = 3.0; // piezoOverhangAmt * 2;
wireDiam            = 1.2;
piezoThick          = 0.25;
wireMargin          = 1.0;

module inner() {
    translate([0, 0, -1]) {
        difference() {
            cylinder(h = 8, r = piezoTolDiam/2);
            union() {
                for ( i = [0:1:2]) {
                    rotate([0, 0, i * 120]) {
                        translate([piezoTolDiam/2, 0, -1]) {
                            cylinder(h = baseThick + 2, r = piezoOverhangAmt);
                        }
                    }
                }
            }
        }
    }
}

module plate() {
    intersection() {
        difference() {
            translate([0, 0, 0]) {
                cylinder(h = 8, r = (piezoTolDiam + wallThick)/2);
            }
            inner();
        }
        cylinder(h = holderHeight, r = piezoTolDiam + wallThick + 1);
    }
}

module wireHole() {
    translate([0, 0, baseThick - (wireDiam/2 + piezoThick)]) {
        rotate([90, 0, 0]) {
            cylinder(h = piezoTolDiam/2 + wallThick + 2, r = wireDiam/2);
        }
    }
}

module all() {
    difference() {
        union() {
            plate();
        }
        for ( i = [0:1:1]) {
            rotate([0, 0, i * 180]) {
                wireHole();
            }
        }
    }
}

// plate();
all();
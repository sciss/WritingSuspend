// Regular OpenSCAD Variables
$fn = 180;// Quality

diaFlansch     =  22.0;
diaHebel       =  20.0;
holeDistHebel  =  14.0;
holeDistFlansch=  15.8;  // XXX TODO
holeDiaMid     =   3.0;  // M3
holeDiaFlansch =   3.0;  // 2.85
holeDiaConn    =   3.0;  // M3
screwHeadDiam  =   6.5;
marginOuter    =   3.0;
marginInner    =   1.5;
diaInner       = max(diaFlansch, diaHebel);
holeDistConn   = diaInner + 2*marginInner + holeDiaConn;
diaOuter       = holeDistConn + 2*marginOuter;
holeDiaHebel   =   3.0;  // M3
plateHeight    =   3.0;  // simulation only

module plate(tpe){
    difference() {
        cylinder(h = plateHeight, r = diaOuter/2);
        translate([0, 0, -1]) {
            union() {
                cylinder(h = plateHeight + 2, r = holeDiaMid/2);
                translate([holeDistConn/2, 0, 0]) {
                    cylinder(h = plateHeight + 2, r = holeDiaConn/2);
                }
                translate([-holeDistConn/2, 0, 0]) {
                    cylinder(h = plateHeight + 2, r = holeDiaConn/2);
                }
                h1 = (tpe == "hebel") ? holeDiaHebel : screwHeadDiam;
                translate([0, holeDistHebel/2, 0]) {
                    cylinder(h = plateHeight + 2, r = h1/2);
                }
                translate([0, -holeDistHebel/2, 0]) {
                    cylinder(h = plateHeight + 2, r = h1/2);
                }
                h2 = (tpe == "flansch") ? holeDiaFlansch : screwHeadDiam;
                translate([holeDistFlansch/2, 0]) {
                    cylinder(h = plateHeight + 2, r = h2/2);
                }
                translate([-holeDistFlansch/2, 0, 0]) {
                    cylinder(h = plateHeight + 2, r = h2/2);
                }
            }
        }
    }
}

projection() {
    plate("flansch");
    translate([diaOuter, 0, 0]) {
        plate("hebel");
    }
}
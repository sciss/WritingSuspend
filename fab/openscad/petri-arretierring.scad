// Regular OpenSCAD Variables
$fn = 120; // 180;// Quality

ringThick           =  3.0;
ringDiam            = 76.0;
blockWidth          = 12.0;
lidHeight           = 25.0;
blockSmall          = 10.0;
blockRest           = lidHeight - blockSmall;
holeWidth           = blockWidth - 1.0; //  + 1.0;
vNew                = true;

module ring() {
    difference() {
        cylinder(h = ringThick, r = (ringDiam + ringThick)/2);
        translate([0, 0, -1]) {
            cylinder(h = ringThick + 2, r = (ringDiam - ringThick)/2);
        }
    }
}

if (vNew) {
    union() {
        ring();
        for ( i = [0:1:2]) {
            rotate([0, 0, i * 120]) {
                translate([ringDiam/2, 0, blockSmall/2]) {
                    cube(blockSmall, center = true);
                }
            }
            translate([(i - 1) * (blockSmall + 2), 0, blockSmall/2]) {
                cube([blockSmall, blockRest, blockSmall], center = true);
            }
        }
    }
} else {
    difference() {
        ring();
        translate([ringDiam/2, 0, 0]) {
            cube(holeWidth, center = true);
        }
    }
}
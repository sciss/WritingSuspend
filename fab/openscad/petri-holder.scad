// Regular OpenSCAD Variables
$fn = 120; // 180;// Quality

petriBaseDiam = 95; // 94
petriHoleDiam = 82; // 78
wallThick = 2.5;
mink = 5;
minkI = 1;
holderHeight = 7; // 6

module inner() {
    union() {
        translate([0, 0, wallThick + minkI]) {
            minkowski() {
                cylinder(h = 8, r = petriBaseDiam/2 - minkI);
                sphere(minkI);
            }
        }
        translate([0, 0, -6]) {
            cylinder(h = 10, r = petriHoleDiam/2);
        }
    }
}

module plate(){
    intersection() {
        difference() {
            translate([0, 0, mink]) {
                minkowski() {
                    cylinder(h = 8, r = (petriBaseDiam + wallThick)/2 - mink);
                    sphere(mink);
                }
            }
            inner();
        }
        cylinder(h = holderHeight, r = petriBaseDiam + wallThick + 1);
    }
}

module clip() {
        minkowski() {
            cylinder(h = 3, r = 50);
            sphere(10);
        }
}

module compare() {
    translate([40, 40, 0]) {
        cylinder(h = wallThick, r = 10);
    }
}

minkNups = 2.0;

module nupsiInner() {
    difference() {
        translate([0, 0, 1.9]) {
            union() {
                minkowski() {
                    cylinder(h = 3, r = max(0.0001, 3 - minkNups));
                    sphere(minkNups);
                }
                translate([-1, -minkNups + 2, 0]) {
                    minkowski() {
                        cube([2, 3, max(0.001, 3 - minkNups)]);
                        sphere(minkNups);
                    }
                }
            }
        }
        translate([0, 0, -1]) {
            cylinder(h = 3 + 2 + 10, r = 1.5);
        }
    }
}

module nupsi() {
    intersection() {
        nupsiInner();
        translate([-3, -3, 0]) {
            cube([6, 6, 3]);
        }
    }
}

plate();
for ( i = [0:1:2]) {
    rotate([0, 0, i * 120]) {
        translate([0, -petriBaseDiam/2 - 3.5, 3]) {
            nupsi();
        }
    }
}
// Regular OpenSCAD Variables
$fn = 32;// Quality

radRingOuter   =  7.5;
radRingInner   =  3.0/2;
plateHeight    =  2.0;
radWireHole    =  1.0;
// marginWireHole =  2.0;
distWireHole   =  5.25;
multiSpace     = radRingOuter * 2 + 2;

module ring(){
        difference() {
            cylinder(h = plateHeight, r = radRingOuter);
            translate([0, 0, -1]) {
                cylinder(h = plateHeight + 2, r = radRingInner);
                for ( a = [0:90:270]) {
                    rotate([0, 0, a]) {
                        translate([distWireHole, 0, 0]) {
                            cylinder(h = plateHeight + 2, r = radWireHole);
                        }
                    }
                }
            }
        }
}

projection() {
for ( i = [0:1:11]) {
    for ( j = [0:1:0]) {
        translate([i * multiSpace, j * multiSpace, 0]) {
            ring();
        }
    }
}
}
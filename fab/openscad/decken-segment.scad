// Regular OpenSCAD Variables
$fn = 180;// Quality

numChans       = 24;
platesPerChan  = 2;
numPlates      = numChans * platesPerChan;
petriOuterDiam = 100.0;
radPetri       = petriOuterDiam/2;
petriSpacing   =  40.0;
segmentAngle   = 360.0 / numChans;
petriExtent    = petriOuterDiam + petriSpacing;
radRingCenter  = numPlates * petriExtent / (2 * PI);  // 214/2 cm
radRingOuter   = radRingCenter + radPetri;
radRingInner   = radRingCenter - radPetri;
plateHeight    =   3.0;  // simulation only
diaHoleHanging =   3.0;
diaHoleConn    =   3.0;
marginHoleConn =  10.0;
servoCaseW     =  23.0;
servoCaseH     =  12.5;
servoAxisOff   =   6.0;

// credits: https://gist.github.com/plumbum/78e3c8281e1c031601456df2aa8e37c6
module sector(h, d, a1, a2) {
    if (a2 - a1 > 180) {
        difference() {
            cylinder(h=h, d=d);
            translate([0,0,-0.5]) sector(h+1, d+1, a2-360, a1); 
        }
    } else {
        difference() {
            cylinder(h=h, d=d);
            rotate([0,0,a1]) translate([-d/2, -d/2, -0.5])
                cube([d, d/2, h+1]);
            rotate([0,0,a2]) translate([-d/2, 0, -0.5])
                cube([d, d/2, h+1]);
        }
    }
}    

module ring(){
        difference() {
            sector(h = plateHeight, d = radRingOuter*2, a1 = -segmentAngle/2, a2 = segmentAngle/2);
            translate([0, 0, -1]) {
                cylinder(h = plateHeight + 2, r = radRingInner);
            }
        }
}

// translate([0,0,0]) sector(30, 20, 10, 90);
//translate([22,0,0]) sector(30, 20, 300, 30);
//translate([0,22,0]) sector(30, 20, 30, 300);
//translate([22,22,0]) sector(30, 20, 10, 190);

moveX = -cos(segmentAngle/2) * radRingInner + 2;

module plate(){
    translate([moveX, 0, 0]) {
        difference() {
            ring();
            translate([radRingCenter, 0, 0]) {
                    cylinder(h = plateHeight + 2, r = diaHoleHanging/2);
            }
            rotate([0, 0, segmentAngle/4]) {
                translate([radRingCenter, servoAxisOff, plateHeight/2-1]) {
                    cube([servoCaseH, servoCaseW, plateHeight + 2], center = true);
                }
            }
            rotate([0, 0, -segmentAngle/4]) {
                translate([radRingCenter, servoAxisOff, plateHeight/2-1]) {
                    cube([servoCaseH, servoCaseW, plateHeight + 2], center = true);
                }
            }
            rotate([0, 0, segmentAngle/2]) {
                translate([radRingInner + marginHoleConn, -marginHoleConn, -1]) {
                    cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                }
                translate([radRingOuter - marginHoleConn, -marginHoleConn, -1]) {
                    cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                }
            }
            rotate([0, 0, -segmentAngle/2]) {
                translate([radRingInner + marginHoleConn, marginHoleConn, -1]) {
                    cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                }
                translate([radRingOuter - marginHoleConn, marginHoleConn, -1]) {
                    cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                }
            }
        }
    }
}

projection() {
    plate();
    rotate([0, 0, 180]) {
        plate();
    }
}
// Regular OpenSCAD Variables
$fn = 120; // 180;// Quality

piezoDiam           = 40.0;
baseThick           = 2.0;
nailDiam            = 7.5;

module plate() {
    cylinder(h = baseThick, r = piezoDiam/2);
}

module nailHole() {
    translate([piezoDiam/2 - nailDiam/2 - 2, 0, -1]) {
        cylinder(h = baseThick + 2, r = nailDiam/2);
    }
}

module all() {
    difference() {
        union() {
            plate();
        }
        for ( i = [0:1:2]) {
            rotate([0, 0, i * 120]) {
                nailHole();
            }
        }
    }
}

// plate();
all();
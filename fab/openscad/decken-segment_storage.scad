// Regular OpenSCAD Variables
$fn = 180; // $preview ? 64 : 180;// Quality
// $fs = 0.5;
// $fa = 0.5;

numChans       = 24;
platesPerChan  = 2;
numPlates      = numChans * platesPerChan;
petriOuterDiam = 100.0;
radPetri       = petriOuterDiam/2;
petriSpacing   =  40.0;
segmentAngle   = 360.0 / numChans;
petriExtent    = petriOuterDiam + petriSpacing;
radRingCenter  = numPlates * petriExtent / (2 * PI);  // 214/2 cm
radRingOuter   = radRingCenter + radPetri;
radRingInner   = radRingCenter - radPetri;
plateHeight    =   3.0;  // simulation only
diaHoleHanging =   3.0;
diaHoleConn    =   3.0;
marginHoleConn =  10.0;
marginClip     =   5.0;
servoCaseW     =  23.0;
servoCaseH     =  12.5;
servoAxisOff   =   6.0;
servoScrewDist =   2.4;

clipWidth  = marginClip * 2;
clipHeight = marginClip * 2 + marginHoleConn * 2;

// credits: https://gist.github.com/plumbum/78e3c8281e1c031601456df2aa8e37c6
module sector(h, d, a1, a2) {
    if (a2 - a1 > 180) {
        difference() {
            cylinder(h=h, d=d);
            translate([0,0,-0.5]) sector(h+1, d+1, a2-360, a1); 
        }
    } else {
        difference() {
            cylinder(h=h, d=d);
            rotate([0,0,a1]) translate([-d/2, -d/2, -0.5])
                cube([d, d/2, h+1]);
            rotate([0,0,a2]) translate([-d/2, 0, -0.5])
                cube([d, d/2, h+1]);
        }
    }
}    

module ring(zAdd = 0.0, radAdd = 0) {
        difference() {
            sector(h = plateHeight + zAdd, d = (radRingOuter + radAdd) *2, a1 = -segmentAngle/2, a2 = segmentAngle/2);
            translate([0, 0, -1]) {
                cylinder(h = plateHeight + zAdd + 2, r = radRingInner - radAdd);
            }
        }
}

// translate([0,0,0]) sector(30, 20, 10, 90);
//translate([22,0,0]) sector(30, 20, 300, 30);
//translate([0,22,0]) sector(30, 20, 30, 300);
//translate([22,22,0]) sector(30, 20, 10, 190);

module plate() {
    translate([moveX, 0, 0]) {
        difference() {
            ring();
            for ( i = [-1:2:1]) {
                rotate([0, 0, i * segmentAngle/4]) {
                    translate([radRingCenter, servoAxisOff, plateHeight/2-1]) {
                        union() {
                            cube([servoCaseH, servoCaseW, plateHeight + 2], center = true);
                            translate([0, servoCaseW/2 + servoScrewDist, 0]) {
                                cylinder(h = plateHeight + 2, r = 0.04, center = true);
                            }
                            translate([0, -(servoCaseW/2 + servoScrewDist), 0]) {
                                cylinder(h = plateHeight + 2, r = 0.04, center = true);
                            }
                        }
                    }
                }
            }
            for ( i = [-1:2:1]) {
                rotate([0, 0, i * segmentAngle/2]) {
                    translate([radRingInner + marginHoleConn, -i * marginHoleConn, -1]) {
                        cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                    }
                    translate([radRingOuter - marginHoleConn, -i * marginHoleConn, -1]) {
                        cylinder(h = plateHeight + 2, r = diaHoleConn/2);
                    }
                }
            }
        }
    }
}

moveX = -cos(segmentAngle/2) * radRingInner + 6;
storageHeightBelow = 37.0; // 38.0;
storageFootWidth = 16.0;
linsenKopf = 3.0; // 2.8;
storageSpacing = 277.0; // 278.0; // 280.0; // 282.0;
storageLength = 120.0;
storageConnThick = 1.5; // 2.0; // 2.0;
storageConnWidth = 8.0;
storageHoleDiam = 2.6; // 3.1;

storageHeightTotal = storageHeightBelow + 2*plateHeight + linsenKopf;

keilLen = 10.0;
keilDepth = 6.5; // 7.0;
keilExt = 7.0;
m25ThreadInsHoleDepth = 5.0;
m25ThreadInsHoleDiam = 4.05;

module ringWithScrews(zAdd = 0.0, radAdd = 0) {
    screwH    = linsenKopf + plateHeight;
    screwDiam = clipWidth * sqrt(2);
    clipW     = clipWidth + 0.5;
    clipH     = clipHeight + 0.5;
    shootH    = 100; // plateHeight + zAdd + 2*screwH;
    union() {
        ring(zAdd = zAdd, radAdd = radAdd);
        for ( i = [-1:2:1]) {
            rotate([0, 0, i * segmentAngle/2]) {
                translate([radRingInner + marginHoleConn, -i * marginHoleConn, -screwH]) {
                    union() {
                        cylinder(h = shootH, r = screwDiam/2, center = false);
                        rotate([0, 0, 45 * i + 90]) {
                            translate([-clipW/2, 0, 0]) {
                                cube([clipW, clipH, shootH], center = false);
                            }
                        }
                    }
                }
                translate([radRingOuter - marginHoleConn, -i * marginHoleConn, -screwH]) {
                    union() {
                        cylinder(h = shootH, r = screwDiam/2, center = false);
                        rotate([0, 0, -45 * i - 90]) {
                            translate([-clipW/2, 0, 0]) {
                                cube([clipW, clipH, shootH], center = false);
                            }
                        }
                    }
                }
            }
        }
    }
}

module clip() {
    rr = diaHoleConn*0.6; // why can't this be 0.5?
    difference() {
        minkowski() {
            translate([-(clipWidth - rr)/2, -(clipHeight - rr)/2, 0]) {
                cube([clipWidth - rr, clipHeight - rr, plateHeight], center = false);
            }
            sphere(rr/2);
        }
        translate([0, marginHoleConn, -1]) {
            cylinder(h = plateHeight + 2, r = diaHoleConn/2);
        }
        translate([0, -marginHoleConn, -1]) {
            cylinder(h = plateHeight + 2, r = diaHoleConn/2);
        }
    }
}


module keil(w = 20, h = 40, d = 10) {
    pt = [ // 0        1          2           3        4          5
        [0, 0, 0], [w, 0, 0], [w, h, 0], [0, h, 0], [0, h, d], [w, h, d]
    ];
    faces = [
//        [1, 0, 3, 2], [0, 4, 3], [1, 2, 5], [1, 5, 4, 0], [3, 4, 5, 2]
        [2, 3, 0, 1], [3, 4, 0], [5, 2, 1], [0, 4, 5, 1], [2, 5, 4, 3]
    ];
    polyhedron(pt, faces);
}

module storage() {
    difference() {
        union() {
            for ( i = [-1:2:1]) {
                translate([0, storageSpacing/2 * i - storageFootWidth/2 + 1*i, 0]) {
                    difference() {
                        cube([storageLength, storageFootWidth, storageHeightTotal - 0.01]);
                        translate([storageFootWidth * 1.5, -0.01, -0.01]) {
                            cube([storageLength - storageFootWidth*3.0, storageFootWidth + 0.02, storageHeightBelow + 2*plateHeight + linsenKopf + 0.01]);
                        }
                    }
                }
            }

            for ( i = [-1:2:1]) {
                translate([0, i * (storageSpacing/2 + storageConnWidth/2), 0]) {
                    translate([0, -storageConnWidth/2, 0]) {
                        cube([storageLength, storageConnWidth, storageConnThick]);
                    }
                }
            }
            for ( i = [0:1]) {
                translate([i * (storageLength - storageConnWidth), 0, 0]) {
                    difference() {
                        translate([0, -storageSpacing/2 - 14, 0]) {
                            cube([storageConnWidth, storageSpacing + 28, storageConnThick]);
                        }
                        union() {
                            for ( j = [-1:2:1]) {
                                translate([storageConnWidth/2, j*(storageSpacing/2 + storageFootWidth/2 + 3), 0]) {
                                    cylinder(h = storageConnThick*3, r = storageHoleDiam/2, center = true);
                                }
                            }
                        }
                    }
                }
            }
            difference() {
                union() {
                    for ( i = [-1:2:1]) {
                        for ( j = [0:1]) {
                            translate([storageConnWidth/2 * i + storageConnWidth/2, 0, 0]) {
                                translate([j * (storageLength - storageConnWidth), i * (storageSpacing/2 + storageConnWidth - 0.01), storageHeightTotal - keilLen - keilExt]) {
                                    rotate([90, 0, i * 90 + 90]) {
                                        translate([0, 0, 0]) {
                                            union() {
                                                keil(w = storageConnWidth, h = keilLen, d = keilDepth);
                                                translate([0, keilLen, 0]) {
                                                    cube([storageConnWidth, keilExt, keilDepth]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                union() {
                    for ( i = [0:1]) {
                        translate([i * (storageLength - storageConnWidth), 0, storageHeightTotal]) {
                            for ( j = [-1:2:1]) {
                                translate([storageConnWidth/2, j*(storageSpacing/2 + storageFootWidth/2 + 3), 0]) {
                                    cylinder(h = m25ThreadInsHoleDepth * 2 + 0.01, r = m25ThreadInsHoleDiam/2, center = true);
                                }
                            }
                        }
                    }
                }
            }
        }
        translate([moveX, 0, storageHeightBelow]) {
            ringWithScrews(zAdd = plateHeight + linsenKopf, radAdd = 0.3);
        }
    }
}

module deckel() {
    intersection() {
        storage();
        cube([storageLength * 2, storageSpacing * 2, storageConnThick * 2], center = true);
    }
}

module blocks() {
    dx = 34;
    dy = 129;
    for ( i = [0:1]) {
        for ( j = [0:1]) {
            translate([i == 0 ? -dx : dx, j == 0 ? -dy : dy, -(storageConnThick + 0.01)]) {
                intersection() {
                    translate([-storageLength/2, 0, 0]) {
                        storage();
                    }
                    intersection() {
                        translate([-storageLength/2, 0, 0]) {
                            translate([-storageLength, -storageSpacing, storageConnThick + 0.01]) {
                                cube([storageLength * 2, storageSpacing * 2, 100], center = false);
                            }
                        }
                        translate([i * -200, j * -200, 0]) {
                            cube([200, 200, 100], center = false);
                        }
                    }
                }
            }
        }
    }
}

//translate([moveX, 0, 0]) {
//    ringWithScrews(zAdd = plateHeight + linsenKopf, radAdd = 0.3);
//}
//keil();
//storage();
//deckel();
blocks();
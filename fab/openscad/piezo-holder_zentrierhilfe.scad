// Regular OpenSCAD Variables
$fn = 120; // 180;// Quality

petriInnerDiam      = 89.0;
piezoDiam           = 40.0 + 3.5; // plus "wallThick" plus tolerance
piezoTol            =  1.0;
piezoTolDiam        = piezoDiam + 2*piezoTol;
piezoOverhangAmt    = 3.0;
piezoOverhangDiam   = piezoDiam - 2 * piezoOverhangAmt;
wallThick           = 2.5;  // falsch gerechnet; ist die halbe !
baseThick           = 2.0;
holderHeight        = 1.0 + baseThick; // 6
armThick            = 3.0; // piezoOverhangAmt * 2;
wireDiam            = 1.2;
piezoThick          = 0.5;
wireMargin          = 1.0;

module inner() {
    translate([0, 0, -1]) {
        difference() {
            cylinder(h = 8, r = piezoTolDiam/2);
        }
    }
}

module plate() {
    intersection() {
        difference() {
            translate([0, 0, 0]) {
                cylinder(h = 8, r = (piezoTolDiam + wallThick)/2);
            }
            inner();
        }
        cylinder(h = holderHeight, r = piezoTolDiam + wallThick + 1);
    }
}

module arm() {
    intersection() {
        difference() {
            translate([0, -armThick/2, 0]) {
                cube([petriInnerDiam/2 + 1, armThick, baseThick], center = false);
            }
            translate([0, 0, -1]) {
                cylinder(h = holderHeight + 1, r = piezoTolDiam/2 + wallThick/2);
            }
        }
        translate([0, 0, -1]) {
            cylinder(h = holderHeight, r = petriInnerDiam/2);
        }
    }
}

module wireHole() {
    translate([0, 0, wireDiam/2]) {
        rotate([90, 0, 0]) {
            cylinder(h = piezoTolDiam/2 + wallThick + 2, r = wireDiam);
        }
    }
}

module all() {
    plate();
    for ( i = [0:1:2]) {
        rotate([0, 0, i * 120]) {
            arm();
        }
    }
}

difference() {
    all();
    for ( i = [0:1:1]) {
        rotate([0, 0, i * 180]) {
            wireHole();
        }
    }
}
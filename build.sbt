lazy val baseName       = "WritingSuspend"
lazy val baseNameL      = baseName.toLowerCase
lazy val projectVersion = "1.4.2-SNAPSHOT"

lazy val buildInfoSettings = Seq(
  // ---- build info ----
  buildInfoKeys := Seq(name, organization, version, scalaVersion, description,
    BuildInfoKey.map(homepage) { case (k, opt)           => k -> opt.get },
    BuildInfoKey.map(licenses) { case (_, Seq((lic, _))) => "license" -> lic }
  ),
  buildInfoOptions += BuildInfoOption.BuildTime
)

lazy val commonSettings = Seq(
  version      := projectVersion,
  homepage     := Some(url(s"https://codeberg.org/sciss/$baseName")),
  scalaVersion := "3.3.1", // "2.13.8",
  scalacOptions ++= Seq("-deprecation"),
  licenses     := Seq("AGPL v3+" -> url("http://www.gnu.org/licenses/agpl-3.0.txt")),
)

lazy val root = project.in(file("."))
  .aggregate(common, shutdown)
  .settings(commonSettings)
  .settings(
    name := baseName,
    description  := "An installation piece",
  )

lazy val common = project.in(file("common"))
  .enablePlugins(BuildInfoPlugin)
  .settings(commonSettings)
  .settings(buildInfoSettings)
  .settings(assemblySettings)
  .settings(
    name := s"$baseName-common",
    description := "Common code",
    libraryDependencies ++= Seq(
      "com.pi4j"      %  "pi4j-core"            % deps.common.pi4j,           // GPIO control
      "de.sciss"      %% "fileutil"             % deps.common.fileUtil,       // utility functions
      "de.sciss"      %% "fscape-lucre"         % deps.common.fscape,         // offline rendering
      "de.sciss"      %% "lucre-pi"             % deps.common.lucrePi,        // GPIO support
      "de.sciss"      %% "model"                % deps.common.model,          // events
      "de.sciss"      %% "numbers"              % deps.common.numbers,        // numeric utilities
      "de.sciss"      %% "patterns-lucre"       % deps.common.patterns,
      "de.sciss"      %% "soundprocesses-core"  % deps.common.soundProcesses, // sound computing
      "de.sciss"      %% "swingplus"            % deps.common.swingPlus,      // user interface
      "net.harawata"  %  "appdirs"              % deps.common.appDirs,        // finding standard directories
      "org.rogach"    %% "scallop"              % deps.common.scallop,        // command line option parsing
    ),
    buildInfoPackage := "de.sciss.writing",
    assembly / mainClass := Some("de.sciss.writing.ServoUI"),
    assembly / assemblyJarName := s"$baseNameL-common.jar",
  )

lazy val shutdown = project.in(file("shutdown"))
  .settings(commonSettings)
  .settings(assemblySettings)
  .settings(
    name := s"$baseName-shutdown",
    description := "Shutdown dispatcher",
    libraryDependencies ++= Seq(
      "de.sciss"   %% "scalaosc"  % deps.shutdown.scalaOsc,
      "org.rogach" %% "scallop"   % deps.common.scallop, // command line option parsing
    ),
    assembly / mainClass := Some("de.sciss.writing.Shutdown"),
    assembly / assemblyJarName := s"$baseNameL-shutdown.jar",
  )

lazy val deps = new {
  val common = new {
    val appDirs         = "1.2.2"
    val fileUtil        = "1.1.5"
    val fscape          = "3.15.5"
    val lucrePi         = "1.11.0"
    val model           = "0.3.5"
    val numbers         = "0.2.1"
    val patterns        = "1.11.0"
    val pi4j            = "1.4"     // incompatible API: "2.4.0"
    val scallop         = "4.1.0"
    val soundProcesses  = "4.14.9"
    val swingPlus       = "0.5.0"
  }
  val shutdown = new {
    val scalaOsc        = "1.3.1"
  }
}

lazy val assemblySettings = Seq(
  // ---- assembly ----
  assembly / test            := {},
  assembly / target          := baseDirectory.value,
  assembly / assemblyMergeStrategy := {
    case "logback.xml" => MergeStrategy.last
    case PathList("org", "xmlpull", _ @ _*)              => MergeStrategy.first
    case PathList("org", "w3c", "dom", "events", _ @ _*) => MergeStrategy.first // bloody Apache Batik
    case PathList("google", "protobuf", _ @ _*)          => MergeStrategy.first // something akka
    case PathList(ps @ _*) if ps.last endsWith "module-info.class" => MergeStrategy.first // bloody Jackson
    case x =>
      val old = (assembly / assemblyMergeStrategy).value
      old(x)
  },
  assembly / fullClasspath := (Test / fullClasspath).value // https://github.com/sbt/sbt-assembly/issues/27
)

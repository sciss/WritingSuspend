// Writing Machine (2011)
30 + 24 + 18 // 72

// Writing (simultan) (2020)
3 * 27 // 81

// 10 cm dishes diametre
// 4 cm inter-dishes spacing

200 * math.Pi / 14 // 45

250 * math.Pi / 14 // 56

300 * math.Pi / 14 // 67

72 = d * math.Pi / 14
val d = 72 * 14 / math.Pi  // 320 cm

// minimale Raumbreite
d + 2*120 // 560 cm

//

val d1 = 54 * 14 / math.Pi  // 240 cm
d1 + 2*120 // 480 cm

val d2 = 48 * 14 / math.Pi  // 214 cm
d2 + 2*120 // 454 cm

///////

54/4.0 // 14 amps

54/3.0 // 18 channels 
54/2.0 // 27 channels

18/4.0 // 5 amps
27/4.0 // 7 amps

360.0/27 // 13.333 degrees per channel

//////

val pico = 4.6 // EUR
// val amp  = 6.85 / 5 // 1.37 EUR
val amp = 9.97 / 5 // 1.99 EUR
val trafo = 11.90 // pffffff

val perChan = amp + trafo // 14 EUR
perChan * 27 // kanpp 400 EUR

27/8.0 // 4x RPi 4B, 4x Logiclink audio-IF

val porto = 29.95
val trafo = 8.46
(trafo * (27-8) + porto) / (27-8) // 9.57 per piece

(trafo * (20) + porto) / (20) // 9.96 per piece
(9.41 * (20) + porto) / (20) // 10.91 per piece


///////////////////

val wire = 320.0
val wireTotM = wire * 48 / 100 // 154 metres
wireTotM / 10 * 3.28 // 50 EUR Kupferlackdraht 0.8mm
wireTotM / 23 * 3.13 // 21 EUR Kupferlackdraht 0.5mm
2.5 * 4 * 24 / 50 * 14 // 67 EUR Stahlseil für Hängung

////////

val da = 219.0
val ang = 360.0/24
val x0 = da/2 * 0.0.toRadians.cos
val y0 = da/2 * 0.0.toRadians.sin
val x1 = da/2 * ang.toRadians.cos
val y1 = da/2 * ang.toRadians.sin
((x0 - x1).squared + (y0 - y1).squared).sqrt // 28.6 cm
math.Pi * da / 24

17.14 / 2 * 24 / 2  // 103 EUR
// 17.14 * 6


val timePer85Cable = 0.5
val timePerPiezoDiscAll = 20.0 // forty minutes for two fully connected piezos; not yet mounted
val timePiezos = timePerPiezoDiscAll * 48 / 60.0 // 16 hours for all piezos
val timePiezosLeft = timePerPiezoDiscAll * (48 - 8) / 60.0 // 13 1/2 hours left as of 19-Sep-23

val timePerLDRCable = 30.0


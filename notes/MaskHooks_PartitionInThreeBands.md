trim is left,bottom,right,top; papersize is {width,height}

```
pdfjam -o mask_hooks-bahn1.pdf --trim '0cm 0cm 145cm 0cm' --papersize '{85cm,230cm}' --scale 1.0 '/home/hhrutz/Documents/devel/WritingSuspend/materials/mask_hooks2_raster.pdf'
pdfjam -o mask_hooks-bahn2.pdf --trim '72.5cm 0cm 72.5cm 0cm' --papersize '{85cm,230cm}' --scale 1.0 '/home/hhrutz/Documents/devel/WritingSuspend/materials/mask_hooks2_raster.pdf'
pdfjam -o mask_hooks-bahn3.pdf --trim '145cm 0cm 0cm 0cm' --papersize '{85cm,230cm}' --scale 1.0 '/home/hhrutz/Documents/devel/WritingSuspend/materials/mask_hooks2_raster.pdf'
```


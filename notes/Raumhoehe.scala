val hoehe_rotor = 375.0
val abhaengung_rotor1 =  20.0
val ziel_hoehe = 160.0
val motor_distanz = 4.3
val laenge_seil1 = hoehe_rotor - abhaengung_rotor1 - ziel_hoehe - motor_distanz // 190.7 cm

val reagenz_untere_hoehe = 178.0 // tiefster punkt
val reagenz_woelbung = 11.5

val laenge_seil2 = reagenz_untere_hoehe - motor_distanz - 3.7 // 170
val abhaengung_rotor2 = hoehe_rotor - ziel_hoehe - motor_distanz - laenge_seil2 // 40.7 cm

val platesPerChan  = 2
val numChans       = 24
val numPlates      = numChans * platesPerChan
val petriOuterDiam = 100.0
val petriSpacing   =  40.0
val petriExtent    = petriOuterDiam + petriSpacing
val kreis_radius_c = numPlates * petriExtent / (2 * math.Pi)
val kreis_diam_c   = kreis_radius_c * 2 // 213.9 cm
val kreis_diam_out = kreis_diam_c + petriOuterDiam // 223.9 cm
val kreis_diam_in  = kreis_diam_c - petriOuterDiam // 203.9 cm

//

val unterHoeheCM    = 40.0  // aktuell gemessen in Reagenz mit den 30cm Seilen, bis untere Kante Servo-Konstruktion
val petriDrahtLenCM = hoehe_rotor - unterHoeheCM - ziel_hoehe  // 175 cm

// piezo wires

val petriBaseHeightCM    = 1.5
val distServoArmSegmCM   = 5.0
val maxRotaDistCM        = petriOuterDiam / 10.0
val piezoWireInnerDistCM = petriOuterDiam / 20.0
val distSegmLuesterCM    = 12.0
val piezoWireLenTolCM    =  1.5
val piezoOuterDrahtLenCM = petriDrahtLenCM + petriBaseHeightCM + distServoArmSegmCM + maxRotaDistCM + piezoWireInnerDistCM + 
  distSegmLuesterCM + piezoWireLenTolCM // 210 cm 

val piezoPiezoDistCM     = 14.0
val piezoInnerDrahtLenCM = piezoPiezoDistCM + 4 * petriBaseHeightCM + maxRotaDistCM / 2 // 25 cm.

// direkt gemessen
val speakerCableCM       = 85.0  // luester to amp

// total need for 0.6 mm kuperlackdraht
// val kupferlackDraht06M = (piezoOuterDrahtLenCM + speakerCableCM) * 2 * numChans / 100.0 // 142 m
// val kupferlackDraht04M = (speakerCableCM * numChans) / 100.0 // 21 m

val kupferlackDraht06M = (speakerCableCM) * 2 * numChans / 100.0 // 40 m
val kupferlackDraht04M = ((piezoOuterDrahtLenCM * 2 + piezoInnerDrahtLenCM) * numChans) / 100.0 // 107 m
val sensorLaenge06M    = (speakerCableCM) * 2 * numChans / 100.0 // 40 m

/////



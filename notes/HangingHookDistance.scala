val platesPerChan  = 2
val numChans       = 24
val numPlates      = numChans * platesPerChan
val petriOuterDiam = 100.0
val petriSpacing   =  40.0
val petriExtent    = petriOuterDiam + petriSpacing
val kreis_radius_c = numPlates * petriExtent / (2 * math.Pi)

val angSegmDeg     = 360.0/numChans  // 15 degrees
val angSegm        = angSegmDeg.toRadians
val x1             = kreis_radius_c * 0.0.cos
val y1             = kreis_radius_c * 0.0.sin
val x2             = kreis_radius_c * angSegm.cos
val y2             = kreis_radius_c * angSegm.sin
val distHakenHaken = ((x1 - x2).squared + (y1 - y2).squared).sqrt // 279 mm or ca. 28 cm

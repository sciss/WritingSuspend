# Notes 26-Oct-2023

## Source material

- we edit to keep the single voice per file, avoid extraneous sounds such
  as doors slamming, dogs barking, kids crying etc.
- we probably should apply some equalisation per file, and a high pass filter
  corresponding to the capacity of the piezos
- audio tracks should be extracted from video files, compressed audio
  converted to AIFF or WAVE, and stereo files converted to mono (selecting the
  clearest channel or making a mix).
- sampling rate should be unified, at 48 kHz which is more common in the video files.

## On the rewriting process

- we pass around 'one phrase', thus the three Pis cannot compute concurrently
- passing around the phrase as a file will cause a lot of network traffic
- it would thus be better to have the entire corpus on the three units, and pass around the
  overwrite instructions, according to which all three units could update their database and
  phrase file
- we don't remove elements from the database, allowing that the occurrence of echoes, as
  they will be rather infrequent, as we use 24 different corpora
- therefore, we do not need to rewrite the database files at all. but we will maintain a
  database 'window'; for instance, if the corpus is twenty minutes long, we use a sliding window
  of say five minutes as a view onto the 'current database'. the sliding window could be simply
  advanced in each iteration (and wrap back at the end of the full database)
- along with the phrase, we keep the 'aging' channel as before
- if we use a RAM disk for the phrase, there is no need to optimise the rewrite, say
  by pooling eight instructions (before the next Pi of the three is invoked)

## Distribution across channels

- we use 24 databases corresponding to 24 individual voices, roughly 12 male and 12 female
  (probably 11 male and 13 female) from six 'continents' (N + S America, Europe, Africa, Asia, Australia).
- we could distribute them formally, e.g. alternating female / male and the continents, or empirically,
  e.g. by creating a shortest path according to resonance correlations (I kind of like that idea)

## Resonance (LDR)

- the idea is to create a resonance file, probably using phase randomisation of the FFT, or using
  the minimum phase approach (e.g. using a real-time convolution with white noise). the file solution
  would be easier on the CPU but heavier on the SD card I/O
- the resonances of each channel are heard based on the LDR signal, using a similar automatic thresholding
  as in Swap Rogues
- the channels adjacent to the current phrase should probably be muted, so when the phrase approaches,
  resonant channels fade out, when the phrase moves away, resonant channels may reappear
- there would have to be an adjustment of levels, aiming for equal loudness between the resonance files

## Motors

- they should reflect the ongoing search process, thus would always be the ones one ahead of the currently
  audible phrase
- they should return to center (0 degree) position which is most relaxed for the wire
- therefore they could go in either direction ; perhaps one for overwrite-select, one for match-select,
  they could map directly to the progress of the process.

----

It might be good to conceive of the phrase itself as circular, which would do away with the issue of
boundary replacement.
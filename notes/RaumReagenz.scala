val rMM = 22.493 * 100 / 2
val dxMM = rMM +  300.0  // 142 cm von der Wand
val dyMM = rMM + 1000.0  // 212 cm von der Wand
val kreis_rad_c = 2139.0/2 // ca. 107 cm vom Mittelpunkt

// 360.0/24
val platesPerChan  = 2
val numChans       = 24
val numPlates      = numChans * platesPerChan
val petriOuterDiam = 100.0
val petriSpacing   =  40.0
val petriExtent    = petriOuterDiam + petriSpacing
val kreis_radius_c = numPlates * petriExtent / (2 * math.Pi)
val segAng         = (360.0/numChans).toRadians
val x0             = kreis_radius_c
val y0             = 0.0
val x1             = math.cos(segAng) * kreis_radius_c
val y1             = math.sin(segAng) * kreis_radius_c
val segDist        = math.sqrt((x0 - x1).squared + (y0 - y1).squared) // 27.9 cm

// quer-linien zur stabilisierung
// ; die idee ist ein weiteres bracket auf die inneren segment-verbinder zu setzen,
//   mit einem weiteren mittelloch, und von dort einen zusaetzlichen draht zum
//   kreismittelpunkt zu ziehen

val radPetri       = petriOuterDiam/2
val radRingOuter   = kreis_radius_c + radPetri
val radRingInner   = kreis_radius_c - radPetri

val hoeheBracketMM = 360.0
val hoeheMitteMM   =  30.0
val marginHoleConn =  10.0
val diaHoleConn    =   3.0

// idee war zur besseren waagerechtheit einen draht diagonal
// vom inneren bracket zum haken im kreismittelpunkt zu
// setzen. letzten endes verworfen

val x2 = 0.0
val x3 = radRingInner + marginHoleConn - diaHoleConn/2
val y2 = hoeheMitteMM
val y3 = hoeheBracketMM

val wireLenDiagonal = ((x2 - x3).squared + (y2 - y3).squared).sqrt // ca. 108 cm

// kommt real nicht hin, ist 3.3 cm zu lang
// d.h. 105 cm nehmen

////////////////////////////////


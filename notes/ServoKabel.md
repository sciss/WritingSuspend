# servo anschlusskabel

- original kabel (ca. 20 cm) in der haelfte d.h. 10:10 cm teilen
- 3-polige litze weiß/gruen/rot auf 100 cm (alt: 102 cm) schneiden
- alle enden 2mm abisolieren
- verloeten: alt rot auf neu gruen, alt gelb auf neu weiß, alt braun auf neu rot
- dabei duennen schrumpfschlauch (D=1.6mm) 6mm aufziehen auf alle drei litzen
- darueber einen groesseren (D=6.4mm) schrumpfschlauch 20mm ziehen und dadurch alle zusammenschnueren
  ; achtung: vor dem verloeten aufziehen!

# decken-aufhängung

- bindedraht 'braun'
- um einen haken legen um öse zu kreieren; ca. 2.5 cm verdrillen
- ab öse 30 cm abmessen und ende mit rotem tape als 'fahne' markieren ; evtl. längere fassung für rotor?
- ca. 6 cm nach der fahne abschneiden
- in die augenmutter einfädeln bis anschlag 'fahne'
- beginnen zu verdrillen, dazu dann die 'fahne' hochschieben

bei verwendung der OBI ring-schrauben, seillänge ca. 5mm kürzer machen gegenüber den M3 Augbolzen

# verbindungskabel gpio - ads

das kabel sollte ca. 6 cm lang sein

- ADS1116 seitig ist die belegung J1 von links nach rechts: GND, SDA, SCL, VCC ; bsp: braun, rot, orange, gelb in einer 4-poligen Dupont-Buchse
- Pi / GPIO seitig: 

# ldr kabel

- von luesterklemme zu terminal-block ads, abhängig von der position (wir machen 'die welle' hier nur bedingt mit:) 110 cm außen, 105 cm mitte-außen, 100 cm mitte-innen ; 95 cm innen ; twisted cable aus 0.4mm kupferlackkabel und braunem bindedraht
- abisolieren der kupferlackdrähte in der länge der aderendhülsen mittels manueller abisolierzange. vor dem crimpen dünn verzinnen 
- ended sind mit kurzen schwarzen schrumpfschlauch ringen markiert (95cm: 1 ring, 110cm: 4 ringe)

## petri base hanging

- mittlerer teil ist 175 cm (marked with flags)
- 4 cm links von erster flag fuer petri holder, 6 cm bis 6.5 cm rights von zweiter flag fuer aufhaengungs-CNC-ring

`MVI_7320.mp4`

new technique: 20 min cabling, 5 min hanging:

- pre-cut c. 190 cm of wire, then straighten it
- tape as before (with 4 and 6cm end pieces, 175 cm in the middle)
- put in petri holder and in hanging ring as before
- now curl up with large diameter and put into a large zip bag along with the hanging-ring-end, close zip close to the petri holder
- proceed with cable two and three

this way we can transport and hang the element without tangling. to hang:

- put petri holder with three zip bags hanging down on the saddle of the V shaped ladder
- take out first cable, make sure it's straight, attach it
- then proceed to cable two and three
- place petri base to produce some weight, double check all cables are correct

## piezo wires

- alle jetzt 0.4mm. innen 25cm, außen je 210cm
- luesterklemme to amp: 0.6mm, length 85cm 

-----

# i2c

basic address of PCA9685 is 0x40
basic possible addresses for ADS1116 are 0x4A, 0x4B, 0x48, and 0x49


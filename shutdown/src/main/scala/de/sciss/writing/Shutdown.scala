/*
 *  Shutdown.scala
 *  (WritingSuspend)
 *
 *  Copyright (c) 2023 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.writing

import de.sciss.osc
import org.rogach.scallop.{ScallopConf, ScallopOption as Opt}

import java.awt.{Dimension, EventQueue}
import java.awt.event.{KeyAdapter, KeyEvent}
import java.net.{InetAddress, InetSocketAddress}
import javax.swing.{JFrame, WindowConstants}
import scala.util.control.NonFatal

object Shutdown {
  case class Config(
                     verbose    : Boolean   = true,
                     isLaptop   : Boolean   = false,
                     nodes      : List[Int] = List(1, 2, 3),
                     oscBasePort: Int       = 0x5753, // 'WS'
                     simulate   : Boolean   = false,
                     keyCode    : Int       = 46,
//                     keyDur     : Int       = 0,
                    )

  private val dotToNodeId = Map.apply[Int, Int](
    36 -> 1,
    42 -> 2,
    30 -> 3,
    78 -> 0,  // laptop
  )

  private val nodeIdToDot = dotToNodeId.map(_.swap)

  def thisIP(): String = {
    import sys.process._
    // cf. https://unix.stackexchange.com/questions/384135/
    val ifConfig = Seq("ip", "a", "show", "eth0").!!
    val ifConfigPat = "inet "
    val line = ifConfig.split("\n").map(_.trim).find(_.startsWith(ifConfigPat)).getOrElse("")
    val i0 = line.indexOf(ifConfigPat)
    val i1 = if i0 < 0 then 0 else i0 + ifConfigPat.length
    val i2 = line.indexOf("/", i1)
    if i0 < 0 || i2 < 0 then {
      val local = InetAddress.getLocalHost.getHostAddress
      Console.err.println(s"No assigned IP4 found in eth0! Falling back to $local")
      local
    } else {
      line.substring(i1, i2)
    }
  }

  def resolveHost(nodeId: Int)(implicit config: Config): String =
    if nodeId == 0 && config.isLaptop then "127.0.0.1" else {
      val dot = nodeIdToDot(nodeId)
      s"192.168.77.$dot"
    }

  def shutdownSelf()(implicit config: Config): Unit = {
    import sys.process._
    if config.verbose then println("Issuing own shutdown")
    val cmd = Seq("sudo", "shutdown", "now")
    cmd.!
  }

  val name: String = "WritingSuspend - Shutdown dispatcher"

  def main(args: Array[String]): Unit = {
    object p extends ScallopConf(args) {

      import org.rogach.scallop.*

      printedName = Shutdown.name
      private val default = Config()

      println(Shutdown.name)

      val isLaptop: Opt[Boolean] = toggle(default = Some(default.isLaptop),
        descrYes = "Run on laptop without GPIO and sensors.",
      )
      val verbose: Opt[Boolean] = toggle(default = Some(default.verbose),
        descrYes = "Verbosity on (default)",
        descrNo = "Verbosity off",
      )
      val nodes: Opt[List[Int]] = opt(default = Some(default.nodes),
        descr = s"Differentiation minimum boost (default ${default.nodes.mkString(",")})"
      )
      val oscBasePort: Opt[Int] = opt(default = Some(default.oscBasePort),
        descr = s"Base UDP port for inter-node communication. To this the node-ids will be added (default: ${default.oscBasePort}).",
      )
      val simulate: Opt[Boolean] = toggle(default = Some(default.simulate),
        descrYes = "Do not actually shutdown this computer itself.",
      )
      val keyCode: Opt[Int] = opt(default = Some(default.keyCode),
        descr = s"Key-code to trigger shutdown, e.g. for Logitech R400: 33 - left, 34 - right, 46 - full screen (default: ${default.keyCode}).",
      )
      verify()

      val config: Config = Config(
        verbose     = verbose(),
        isLaptop    = isLaptop(),
        nodes       = nodes(),
        oscBasePort = oscBasePort(),
        simulate    = simulate(),
        keyCode     = keyCode(),
      )
    }

    implicit val c: Config = p.config

    if !c.isLaptop then {
      // disable screen blanking
      import sys.process._
      Seq("xset", "s", "off").!
      Seq("xset", "-dpms").!
    }

    run()
  }

  def run()(implicit config: Config): Unit = {
    import config.verbose

    val oscCfg = osc.UDP.Config()
    if config.isLaptop then
      oscCfg.localIsLoopback = true
    else
      oscCfg.localAddress = InetAddress.getByName(thisIP())

    val t = osc.UDP.Transmitter(oscCfg)
    if verbose then t.dump()
    t.connect()

    val targets = config.nodes.map { nodeId =>
      val host = resolveHost(nodeId)
      val port = config.oscBasePort + nodeId
      new InetSocketAddress(host, port)
    }

    def issueShutdown(): Unit = {
      if verbose then println("Issuing shutdown")
      val m = osc.Message("/shutdown")
      targets.foreach { target =>
        if verbose then println(s">> $target")
        try {
          t.send(m, target)
        } catch {
          case NonFatal(ex) =>
            ex.printStackTrace()
        }
      }
      if config.simulate then sys.exit() else Shutdown.shutdownSelf()
    }

    EventQueue.invokeLater(() => {
      val f   = new JFrame(Shutdown.name)
      f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
      val cp  = f.getContentPane
      cp.setPreferredSize(new Dimension(480, 320))
      f.pack()
      f.setLocation(32, 32)
      f.setVisible(true)
      f.toFront()
      cp.requestFocus()
      cp.addKeyListener(new KeyAdapter {
        override def keyPressed(e: KeyEvent): Unit = {
          val kc = e.getKeyCode
          if verbose then println(s"Key-code: $kc")
          if kc == config.keyCode then issueShutdown()
        }
      })
    })
  }
}

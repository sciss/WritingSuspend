# Writing (suspend)

This repository builds the third reconfiguration of the piece 'Writing Machine', this time attempting a single circle
configuration suspend from motors, and based on three miniature computers interleaving twenty-four channels that could
be activated in parallel based on the audients' movement. See [Research Catalogue](https://www.researchcatalogue.net/view/665526/665527)

Unless specified differently in the sub-projects, all code and materials are
(C)opyright 2023 by Hanns Holger Rutz. All rights reserved. This project is released under the
[GNU Affero General Public License](https://codeberg.org/sciss/WritingSuspend/src/branch/main/LICENSE) v3+ and
comes with absolutely no warranties.
To contact the author, send an e-mail to `contact at sciss.de`.

With respect to Mellite workspaces here (re)produced, all rights are reserved.
No reuse or changes permitted without explicit written consent.

With respect to all fabrication files (svg, stl, kicad, scad etc.), all materials licensed under
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

## building

`sbt assembly`

## running on Pi

For just two nodes:

    java -cp common/writingsuspend-common.jar de.sciss.writing.WritingSuspend --nodes 1 2

Testing the servos:

    java -cp common/writingsuspend-common.jar de.sciss.writing.ServoUI

## fix wiring-pi

__Important:__ Wiring-Pi is broken on the Pi 4. The pull up/down resistors cannot be configured.
See https://pi4j.com/1.3/install.html#WiringPi_Native_Library -- one needs to replace the installed versions
with an unofficial one!

    sudo apt remove wiringpi -y
    sudo apt install git-core gcc make
    cd ~/Documents/devel/
    git clone https://github.com/WiringPi/WiringPi --branch master --single-branch wiringpi
    cd wiringpi
    sudo ./build

## running GPIO code without `sudo`

See http://www.savagehomeautomation.com/projects/pi4j-now-supports-non-privileged-access-no-more-rootsudo.html

`GpioUtil.enableNonPrivilegedAccess()`

or

`export WIRINGPI_GPIOMEM=1`

## multi-node laptop test

Like so: First node =

    --is-laptop --dir-work /tmp-folder1 --debug --node-id 1 --nodes 1 2 --jack-client-name client1

Second node =

    --is-laptop --dir-work /tmp-folder2 --debug --node-id 2 --nodes 1 2 --jack-client-name client2

## logitech presenter

Used to control mute and shutdown of the installation. `KeyState` key codes (desktop):

- 112 'left' ; possibly used for decrease volume
- 117 'right' ; possibly used for increase volume
- alternating between 9 and 71 'play' ; possibly used for mute/unmute
- 60 'full-screen' ; we'll use that for shutdown

## channel mapping

From logical audio channel output to actual circle sound

- 1: 2
- 2: 1
- 3: 6
- 4: 5
- 5: 8 (on node one very faint; possible cable issue; 'center channel')
- 6: 7
- 7: 4
- 8: 3

Thus, Jack source client cables should be sorted as follows: 2, 1, 8, 7, 4, 3, 6, 5
Thus, Jack source client cables should be sorted as follows: 2, 1, 6, 5, 8, 7, 4, 3
